## [1.3.1](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.3.0...v1.3.1) (2025-01-26)

### :bug: Fixes

* update prices ([d4eba05](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/d4eba0570552e3c315cc1101e54cfa2e11151949))

## [1.3.0](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.15...v1.3.0) (2025-01-14)

### :sparkles: Features

* add events for 2025 ([3368af3](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/3368af330b021355cfc3bc26eb01c5eca0b0a824))
* update CI/CD configuration and add semantic release setup ([57295ae](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/57295ae2004846c69eaaa15f1b7ec218bd8b86c9))

### :bug: Fixes

* change time notation of kirchenkonzert ([fc3c3bf](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/fc3c3bff27423624269a39d1865d9f5a56189131))
* update hero image for Jugend In Konzert 2025 ([3743864](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/37438643a1e93e88276b27bd19ec804e06dd8096))
* update latest versions ([59db657](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/59db6574e78288340ab6dd915ba47cccfbc11b3f))
* update musiker ([1e9c0ae](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/1e9c0aee2b2d47b57b35bea6bf0ea6e46b0deea6))
* update musiker reference and add new musician profile for Katharina Jakob ([c790df8](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/c790df8c230fc351497a8b68a1a2b8845d01b7c6))

### :repeat: CI

* change deps and needs ([57ad9ee](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/57ad9eef36c86080bdcf7c363e16ef25cd0ea7d5))

## [1.2.15](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.14...v1.2.15) (2024-07-19)

### :bug: Fixes

* Update to newest version of astro 4.12 ([4eb661d](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/4eb661d733b6d7969593ab30015b2e5fdc320290))

## [1.2.14](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.13...v1.2.14) (2024-05-13)


### :bug: Fixes

* Update dependencies ([ae87e43](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ae87e431c5acb72f4deed886e629a0d0631f5d02))
* Update Serenade 2024 ([ac45c62](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ac45c62fc4201b773c826e54d249ba538948bb3b))

## [1.2.13](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.12...v1.2.13) (2024-05-09)


### :bug: Fixes

* typo of serenade on mobile ([2640718](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/2640718ceee2d6887cd410c0feb911dab61e90a8))


### :zap: Reverts

* Remove disabled pipeline ([d51bac6](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/d51bac6adb6166ca24c42ecb3726e969c51ea402))


### :repeat: Build System

* Update dependencies ([f65368a](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/f65368aa9f5624c602cbfe581c1879308421968f))

## [1.2.12](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.11...v1.2.12) (2024-04-22)


### :bug: Fixes

* update readme ([bbe11e9](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/bbe11e9da9336fb787b9c3e3f9af32054c1f402f))

## [1.2.11](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.10...v1.2.11) (2024-04-04)


### :bug: Fixes

* Update Image of Vatertagsfest 2024 ([265ee5e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/265ee5e2e21aa37992318dae29ed5d23fbc3c52a))
* Update Image of Wertungsspiele 2024 ([ca1e9cb](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ca1e9cb13e620074ea40016cd4d1e82186f3b540))


### :repeat: Chores

* LICENSE hinzufügen [skip ci] ([87516fe](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/87516fe6316ffef48d8e22118c596795288b2e64))


### :repeat: Build System

* Update to Astro 4.5 ([f4d678f](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/f4d678fd8b43add30c8606ca3558471f67870885))

## [1.2.10](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.9...v1.2.10) (2024-02-02)


### :bug: Fixes

* Change image of teenieparty_2024.jpg ([4ea40a7](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/4ea40a7ada468c73e950e7abae05383089151478))

## [1.2.9](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.8...v1.2.9) (2024-02-01)


### :bug: Fixes

* Typo gefixt ([d77f2b1](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/d77f2b1726a746b88ba15ea3506dc136c5689b3a))

## [1.2.8](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.7...v1.2.8) (2024-02-01)


### :bug: Fixes

* Ändere Ort von Teenieparty ([588c7f7](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/588c7f7871a9cae4fb913d270990b082e257b076))

## [1.2.7](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.6...v1.2.7) (2024-02-01)


### :bug: Fixes

* Anpassung der Inhalte für In-Konzert ([ea609e2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ea609e2972f0c45e4baacc0af44c70e25398370a))
* Erweitern um Teenie-Party ([8ea46d2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/8ea46d2da9e262d63f62fb4b4436323b7e64c89b))


### :repeat: Chores

* Update dependencies ([91ed455](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/91ed455075da06e3d1f20718d2cdda4d397e9bff))

## [1.2.6](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.5...v1.2.6) (2024-01-27)


### :bug: Fixes

* Change image of in konzert ([83ac485](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/83ac485ff29b353de4b7d0813283121d288b485d))

## [1.2.5](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.4...v1.2.5) (2024-01-12)


### :bug: Fixes

* Change title of news ([4e4da83](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/4e4da835552a2be57587ce6000d03efbf1bc6c48))

## [1.2.4](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.3...v1.2.4) (2024-01-12)


### :bug: Fixes

* Add new blog to announce in konzert ([0359702](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/0359702cd91907cddf9be955982f8b83dfb835b6))
* Add new in-Konzert-Data ([81f70b2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/81f70b243d11190c2868651f58c39f69ef9efe09))

## [1.2.3](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.2...v1.2.3) (2023-11-30)


### :bug: Fixes

* Hinzufügen von Fasching ([a6a19f0](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/a6a19f04c98cf84d83c35690d8f92a75f19ac657))
* in_konzert 2024 final eingefügt ([9eff111](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/9eff11162718cc055e40e721fe66ebf933e86d6d))


### :repeat: Chores

* Austausch von Konzert-Bildern ([b3d04bf](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/b3d04bfc4f5a2652effcc9d0e66e4c7abbccd4db))

## [1.2.2](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.1...v1.2.2) (2023-10-30)


### :bug: Fixes

* Änderungen am Casinoabend 2023 aktualisiert ([d66b96b](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/d66b96be63329cfd89c48963cac91ac126860708))

## [1.2.1](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.2.0...v1.2.1) (2023-10-24)


### :bug: Fixes

* Optimierung der Darstellung von Abgesagten oder Verschobenen Veranstaltungen ([8a93f8d](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/8a93f8dff55d599963e07fb92fe5d507d8d9eb64))

## [1.2.0](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.15...v1.2.0) (2023-10-24)


### :sparkles: Features

* Verschoben und Abgesagt-Tags implementiert ([c5065d3](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/c5065d3a9d5f36da4279e0de97bb42b113120a3c))


### :bug: Fixes

* SCSS-Probleme korrigiert ([acbb616](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/acbb616c9d41d1fd9993faba2564e9fffc3874c2))
* Update zu Astro 3.3 ([9216d10](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/9216d109f50de628d5c1cef05b49d5fbbd2f9e36))

## [1.1.15](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.14...v1.1.15) (2023-10-17)


### :bug: Fixes

* Korrektur von Zeiten ([9f4dd51](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/9f4dd51bee9a36263e380e6e61001a7f1548263b))


### :repeat: Chores

* Reduzierung der Assets ([9d912f2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/9d912f29e43ba0f562bb95c719edbe0ff5255983))

## [1.1.14](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.13...v1.1.14) (2023-10-08)


### :bug: Fixes

* Bericht für die Radl-Tour eingetragen ([ce3f265](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ce3f265f1cf91faa258cbd82f18cfc218bfa3f32))
* Casino-Night eingetragen ([a9e8bf5](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/a9e8bf579c27ea62e4dad98237a496a15689dfc2))


### :repeat: Chores

* Update to new Astro-Version ([a5b02d9](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/a5b02d90f73055fc1c77b876181d52b565cf3f6f))

## [1.1.13](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.12...v1.1.13) (2023-09-07)


### :bug: Fixes

* Whitespace Fehler korrigieren ([1a1251e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/1a1251e91243e944f26fad078beec901f3d22560))

## [1.1.12](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.11...v1.1.12) (2023-09-07)


### :bug: Fixes

* Bezirksmusikfest hinzugefügt ([8dd0a32](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/8dd0a3293c5db80112ce0122e8574f8eee54f735))
* Hinzufügen von Vereinintern / Allgemein in Menü aufgenommen ([ffff58b](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ffff58bcd06ce5bffcf1330199c2189ca05edd6c))
* Korrektur von Dokumenten zum Download ([ec2d754](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ec2d754499838131f88c32d209b387f17c56ba61))

## [1.1.11](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.10...v1.1.11) (2023-09-05)


### :bug: Fixes

* Korrektur von Event-Filter (Fehlerhaft) ([73a6bbf](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/73a6bbf63bca981e060c1967b59bd4e1c5cc89e6))

## [1.1.10](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.9...v1.1.10) (2023-09-05)


### :bug: Fixes

* Remove Transition ([7522f46](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/7522f463b1d3df810c541428c9e861465f0ffd61))

## [1.1.9](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.8...v1.1.9) (2023-09-05)


### :bug: Fixes

* Korrektur von Einträgen für Build ([e797645](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/e797645bd9b4dd3e65bc44afcf38ca2695dfa34c))
* Updaten zu Astro 3.0 und bestehende Einträge migrieren ([bd1382c](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/bd1382c73cd52f64f95647408688b1acd753e59a))


### :repeat: Chores

* Wechsle zur fixen Version von Astro ([110e12a](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/110e12a2fa512c409d770cc0d16ae44612d2ffa6))

## [1.1.8](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.7...v1.1.8) (2023-08-13)


### :bug: Fixes

* Add Kinderfasching 2018 ([259920e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/259920efd2f98997a5c1dbef72920c8b277cd618))
* Add Konzert 2018 ([8cb83f9](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/8cb83f9374e0d401b0aeee3822ba4a48056edb48))
* Add Oktoberfest 2018 ([7ed1944](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/7ed19448d9d170535d80b87217db5652f55a1a81))
* Add Serenade 2018 ([4909543](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/49095436b06b8ce3ddb49d6dfc7bfb98fe0dda5a))
* Hinzufügen von Kinderfasching 2017 ([1f5db70](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/1f5db708464eec7991c2590f1198957831fd127d))
* Hinzufügen von Serenade 2017 ([1633316](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/163331621cb56c899f2455874c25b0bcaad56e5e))


### :zap: Reverts

* Entfernen von 2005 bis 2007 von Webseite ([320f011](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/320f01157cccb3cf2f7f27b814b1a99abc13f948))


### :repeat: Chores

* Einfügen von Bildern des Bezirkmusikfestes 2016 (Vorbereitung) und Aufsplittung auf die einzelnen Tage ([1c61ad0](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/1c61ad0b617b354a65f800fedbbd7c382ec70e96))


### :repeat: Build System

* Umstellung auf neusten Build von Compressor ([52702dd](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/52702dda4b983e6e959c28e30211bf133da158cc))

## [1.1.7](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.6...v1.1.7) (2023-08-08)


### :bug: Fixes

* Rebuild with astro-compress ([0532802](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/053280246fed22d7fa5b8612c8775f227e315cb3))


### :repeat: Chores

* Update lock-file ([3784868](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/3784868fd8942eeada9abb9f8b203301fc372011))

## [1.1.6](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.5...v1.1.6) (2023-08-06)


### :bug: Fixes

* Temporäres entfernen von Dependencie, bis diese von npm wieder verfügbar ist ([bb5898d](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/bb5898d456ca33e01c715034733bf5d477cf95a6))

## [1.1.5](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.4...v1.1.5) (2023-08-06)


### :bug: Fixes

* Korrektur von Bildname ([03ec202](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/03ec2027b4dbbed3560384868ecb407a028f4334))
* Navigation für Mobil korrigiert ([72c9e98](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/72c9e98e41c6947ffe05407fef3ce4b9576ec054))

## [1.1.4](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.3...v1.1.4) (2023-08-05)


### :bug: Fixes

* Korrektur von Musikern ([d6e99e5](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/d6e99e5315e2fb0aed204d051f065a8055041b41))
* Neuer Eintrag: InKonzert 2019 mit Berichte ([4043f48](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/4043f485f3cf78190f9a6c8470fbcb2ce3fd8d3f))
* Neuer Eintrag: Kinderfasching 2019 mit Berichte ([1c28ef1](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/1c28ef14c8d3c87d531e322b9dc7cb11dc2c0ed6))
* Neuer Eintrag: Kirchenkonzert 2019 mit Berichte ([8c4553d](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/8c4553d3d7126e8fc67b6b35efb4f5ff6ea6ecc2))
* Neuer Eintrag: Vatertag 2019 mit Berichte ([5174b96](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/5174b964ad22a73a3bbc7e3f1feeda5c1542ecb7))

## [1.1.3](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.2...v1.1.3) (2023-08-05)


### :barber: Style

* Optimieren von Blog-Darstellung der Bilder ([1877d3c](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/1877d3cc7de3c9f3acddf3746977aebd317b2a4f))


### :repeat: Chores

* Neuesten Stand sichern ([7c67597](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/7c67597adaf6911175802a3d184b334d8dc8eb51))


### :bug: Fixes

* Fehler bei Sortierung der Events korrigiert ([c90ddd5](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/c90ddd5dec78797aad618410ca8790d6475af2ab))
* Hinzufügen von Verlinkung zur Jugend auf Hauptseite ([f8cf811](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/f8cf81112dc215385a0b6bbd05950b8377dcca72))
* Neuer Eintrag: 2020 Abschied Dirigentin ([ef8463e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ef8463e85330c275fd75fc37fc60c3ce055a7876))
* Neuer Eintrag: 2020 Fasching mit Berichten ([21ecbb8](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/21ecbb83576c981f874888acc2ba6e91dba2f731))
* Neuer Eintrag: 2020 InKonzert mit Berichten ([420648f](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/420648fae9e480e063a6a2156b8974ed72dd6d29))
* Neuer Eintrag: 2020 Neuer Dirigent ([bad48bd](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/bad48bd2d312d0248132944ad620a48fe057085f))
* Neuer Eintrag: 2020 Serenade mit Berichten ([a7cbc26](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/a7cbc26f6388bc6ce7236bbf913630294e738e7f))
* Neuer Eintrag: 2021 Konzert mit Berichten ([5d2e89e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/5d2e89e74a6ed35cbbe46c46f7b452457a2646f6))
* Neuer Eintrag: 2021 Serenade mit Berichten ([b0e6b8c](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/b0e6b8cb760d3e115fb816e14a6213867b5b2c1a))
* Neuer Eintrag: 2022 neue Vorstandschaft ([0832f55](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/0832f55c9b8cf40f103c299197708964e3edaf97))
* Neuer Eintrag: 2022 Serenade mit Berichten ([70d01be](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/70d01bebf62aa37020b33d17e93328201ee0ddcf))
* Neuer Eintrag: 2022 Serenade mit Berichten ([993b947](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/993b947d7345e55031bcff1ba944c3d018f993ce))
* Neuer Eintrag: 2022 Vatertag mit Berichten ([6b2820f](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/6b2820f09279cfc19edc85dd791bd70440eea995))
* Neuer Eintrag: 2023 Kinderfasching mit Berichten ([5cb55f9](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/5cb55f999b6eb7c0a348c0c0c56b189efb7c3a07))
* Neuer Eintrag: 2023 Vatertag mit Berichten ([919bac2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/919bac217ba4051d8547532838ea575034efa42c))
* Rebuild with Image-Paths ([a49e549](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/a49e549d861be68ec1d8567d93b62ad318326fc5))
* Sponsoren optional gemacht ([04bc477](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/04bc4778038b00c7ca77c6dfccc5187ec55405b7))
* Update InKonzert 2024 ([cc77753](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/cc77753436af1cd47418c04da0a06601c5d0d81e))
* Update Kirchenkonzert 2023 ([ff7a56d](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ff7a56d5342aa692460bbe00c1db881ea0e14332))
* Update Serenade 2023 ([f1f8a5a](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/f1f8a5a0fb1a803d8447a82c55826aa3a5596752))
* Update Tour de Lech 2023 ([7850477](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/7850477e6209f138090890e0f274e3447012e477))

## [1.1.2](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.1...v1.1.2) (2023-07-31)


### :repeat: Build System

* Change Routers ([cace698](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/cace69897a195c385728cfe4b532726940158d38))
* Change Routers ([adf9b3d](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/adf9b3d3b3bcf47fde2d245919dfe5006a3fb4e4))
* Fix URL ([1695f3e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/1695f3e8aa588feebc27265d6ae04ce65ef344d4))
* Optimize Build ([bbebc88](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/bbebc88ac224d4405a30e5bd1bcc18bbd85200a5))
* Rebuild URL-Management for traefik ([cb09561](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/cb09561a9f34fd6e2243f0049f290ee4bdb66602))


### :bug: Fixes

* Neue Jugend-Dirigentin hinzugefügt ([334118b](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/334118b15b0c90953b4f3b6ad5cdd0652b7d8369))

## [1.1.1](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.1.0...v1.1.1) (2023-07-27)


### :bug: Fixes

* Liveschaltung der Webseite und Typofixes ([336c9f2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/336c9f298fa04b32117b0f55e76296f81a2820cb))
* Wertungsspiele ausgetauscht ([c48b40a](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/c48b40aff85fa0441e8db003366279a72890ab3a))

## [1.1.0](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.0.4...v1.1.0) (2023-07-27)


### :barber: Style

* Optmiere Style für Prose-Images ([16a99d0](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/16a99d0199f34e655f1f04c6fa9291bf2b9de69d))


### :bug: Fixes

* Bilder von der Serenade eingefügt ([ac9f759](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ac9f75990064dca2bab958dfe550df6c861898ce))


### :sparkles: Features

* Bericht zum Abschied von Wolfang eingefügt und Loadings gefixt auf der Startseite ([74992e1](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/74992e140e3012b416e10319c999564404c658b3))
* In Konzert 2024 eingefügt ([112a5d3](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/112a5d3ec890e0e3b746854e42b5da93af027341))
* Serenade 2024 eingefügt ([c78ebf9](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/c78ebf9264d6ed4710e2a26eb148e17be107edbf))
* Tour de Lech eingefügt ([bc17ba2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/bc17ba200f786240f3c4045373efeb3f15abab41))
* Vatertag 2024 eingefügt ([e97867a](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/e97867a094c5699cb728ea2638fe65397b380b35))
* Wertungsspiele eingefügt ([6597311](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/6597311f89b85754784d4c05b21998d6d6ab6e20))

## [1.0.4](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.0.3...v1.0.4) (2023-07-21)


### :bug: Fixes

* Add Favicons ([b191ba2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/b191ba292c270e5fcb8a78d76850b03a3dbb580c))


### :repeat: Chores

* Optimiere 404-Handling für Favicon ([257e1bb](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/257e1bb5f9b0333da931159bc6fc791c298011bc))

## [1.0.3](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.0.2...v1.0.3) (2023-07-21)


### :bug: Fixes

* Jugendseite fertig gestellt ([54d9c60](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/54d9c60472de53aa053484fef005bbcce89112e6))


### :repeat: Chores

* Update to Astro 2.9 ([9942e3e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/9942e3ef1b1d9632d35fd1e83b554dff20a24005))

## [1.0.2](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.0.1...v1.0.2) (2023-07-18)


### :bug: Fixes

* Bild für anorld_jugidth eingefügt ([e65f9d6](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/e65f9d648a12417dbde8556b22233414d45fd128))
* Bild für geissler_ria eingefügt ([f494722](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/f4947229637dbd770e6cef0e5fc72ca8ff83d59b))
* Bild für haggenmueller_bernhard eingefügt ([e426964](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/e426964bb69fcde15a762ac24a39d01efca83763))
* Bild für herz_sebastian eingefügt ([6caa6ed](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/6caa6ed085fca3be9c26430ae707a1c7300c0042))
* Bild für kopf_barbara eingefügt ([9bc7a55](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/9bc7a55e5674b1deb06fcf15790f074ef395333e))
* Bild für springer_melanie eingefügt ([9fac388](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/9fac388b31a5359ba21b5b9940bad18f866d5958))
* Bild für wiblishauser_eva eingefügt ([89c247e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/89c247e14ba366eb5e987ad5141f787cb1741bd5))
* Bilder für die Musiker gawron_bernhard, hamparian_julia, jannasch_carolin, lauter_lina, schneider_ingo, wegele_norbert, weihmayer_georg, weihmayer_vincent, wiblishauser_anna aktualisiert ([e765f00](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/e765f007b7bb8479c44633102d7e0ff05309b456))
* Richtige Version in Footer eingebaut ([7d34c2f](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/7d34c2fe1929c39c2611b676d4c1af08468e3371))

## [1.0.1](https://gitlab.com/forestdesign/mvobermeitingen/website/compare/v1.0.0...v1.0.1) (2023-07-18)


### :repeat: Chores

* Remove old project ([16a916f](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/16a916fcd4fc3ea05860fb39440602a9b61713c0))


### :bug: Fixes

* Bild für luedecke_michaela aktualisiert ([41e5c97](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/41e5c97b87a5435d1bca8f81f27e1e205ebeeb1b))
* Bild von rid_daniela aktualisiert ([e113cdd](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/e113cddcd40609d9dafd1590e771242e131dca36))
* Bild von rid_johann aktualisiert ([1b24a65](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/1b24a65f6b6ea19087f8951f99499796de77a94e))
* Bild von schiegg_thomas aktualisiert ([4fe7057](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/4fe7057810ab436ae8d1cfa248fc83bbad1dd9d7))
* Korrektur der Darstellung von Musikern, damit diese nicht gestreckt werden ([f8916a0](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/f8916a072d13fd543637a0bacabebc777ed07ebd))
* Licht für lichtinghagen_karl eingefügt ([d20eef0](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/d20eef0cacd2eb1cb6f191a3e35deedc42c1fdf1))
* Saxophone/Baritonsax durch Tenorsaxophon und Barisaxophon ersetzt ([3e7f8ab](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/3e7f8abad3bc9c0d1b002b7563214ce5ed818282))

## 1.0.0 (2023-07-18)


### :memo: Documentation

* Beschreibung der Seiten eingebaut ([7618a35](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/7618a350d5d388b520794837aa33489a0b15a2fa))


### :repeat: Chores

* Add Network to Compose ([6335e99](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/6335e99ca4436ff68f730461585ad575165e9bd5))
* Add Network to Compose ([fba1d4a](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/fba1d4a224f951f5ff50f07d20b2bd03451cbe98))
* Add Network to Compose ([7c8278e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/7c8278ef057c60bd06efef9bf8d48f61ab7cd730))
* Add Network to Compose ([34aad52](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/34aad522903ef08bd98cd47e5a78ae71805b6d05))
* Add Network to Compose ([5c8ef87](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/5c8ef87c27d6facda0e355034757303c684f85cc))
* Add Network to Compose ([da828ef](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/da828ef5a870e67e06e173c54f0b3c8905171b30))
* Add Server-URL ([c576e3d](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/c576e3d3621558ae19a368d39a56b4c6534c79f1))
* Remove unused Host ([0585f33](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/0585f335223995592f7c0f911add2e99f6f82f44))
* Remove unused Host ([305a9c1](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/305a9c135a5d94d0cb5f8f5e5e372e527502dd6f))
* Update für Traefik ([43e1d88](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/43e1d88d7b40651c30ffa8a4ac935e0ec9e4fd40))


### :repeat: Build System

* Change Port ([ac6c615](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ac6c6153e3c2312bfb8f1d52e7632f71dad03881))
* Change Port of Container ([3b97cdb](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/3b97cdbf91858e1b948f24038fd517d357182be4))
* Open Volume ([26160d2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/26160d2c24b2487383cba8e463e51724fc1ab2c7))
* Überschreibe nur nginx-conf und Suppattern ([fc9058d](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/fc9058d0b8b34a2a5253d1da3f97808f80bb5d27))


### :sparkles: Features

* Aufbau von Event-Seiten und Home ([d691515](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/d6915159478e506cd72c781298bd75a0ce8ccf12))
* Erstelle Events, nächste Konzerte und den Hero ([d85edd7](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/d85edd7041e4fde342de67b689b6b1ecce7072c3))
* Erweitern um Media-Service ([8095e5a](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/8095e5abe81c852a20ec975c8e7a8204bcd9b900))
* Eventservice gebaut ([d4587c1](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/d4587c1da55cc720451d6c2291afd1def3d90b86))
* Implement SSR ([713d3ca](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/713d3ca9547eef3ccfcc13028cf7c2b214646cea))
* Implementiere Astro.js als Grundlage der Webseite ([2c04654](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/2c04654de9b1fe5e6182eef27d7d929d432db09d))
* Implementieren von Event-Ansichten ([93983be](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/93983be0689556c0c283c94d71a2adb2b5ed7d44))
* Implementieren von Neuigkeiten und Pflichtangaben ([7edeb8d](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/7edeb8d49f1fb524608d6f79b4c6e320163761fc))
* Musiker und Vorstände aktualisiert ([0caf0e2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/0caf0e26c87c3b5469106f7109d628fddbafadf1))
* Reduce to live reload ([2af1050](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/2af1050f520f9a366fef63c1169ae71f3a14e749))
* Update Timecountdown ([3446ca0](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/3446ca0f4701de4272a97efbfb7e21b96d2f87cb))
* Vorstandschaft und Musiker eingefügt ([c93d2dc](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/c93d2dc4f69c0bb5bd587292d50debf1238ed431))
* Weitere Inhalte eingebaut für 2006 und 2023 ([7e2b817](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/7e2b8173ecf6034194d4a2b87ecef9db5588778f))


### :bug: Fixes

* Add Install to Entrypoint ([6850836](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/6850836454282991a88f19eff351f5b0e7289076))
* Change Apts ([ba599ed](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/ba599eda1b296b5f5189441017f5daef7a52e045))
* Change Entrypoint for solid startup ([b586055](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/b5860550a6182cc16542e72af45b7ae1b88e3b47))
* Change Node ([fc5d0be](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/fc5d0be0ed32b513b7ed99970ab12a630e86d38b))
* Document injection ([f785952](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/f785952ef92aeebeb56c02e230b337bb4a905091))
* Integriere nginx.conf und die Sup-Petter ([e56112b](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/e56112b5067d2c55cf62edbb1c51af71f0c18b5f))
* Interpretiere png ([8d9e12e](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/8d9e12e4100e3018723274d60243cf7e02b47657))
* Korrektur von Farbverwaltung ([915bf60](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/915bf6082c13319867fdc27a33ff3525d9392442))
* Korrektur von navigation ([f1f85ab](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/f1f85ab16ac5674ab626c6def6e1db28b419f354))
* Korrigiere inkompatible Version ([8ed3b92](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/8ed3b92ba5adb2d8e2262d80bfa14f4439fdf2b7))
* Leerzeichen entfernen ([a8c39d2](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/a8c39d286feb68a95884806cdba68125c9dae2e8))
* Optionale Abfrage korrigiert ([dd0ada3](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/dd0ada3e1f54ac65092ad2d82ebd798e9e73685b))
* Unnötigen path entfernt ([fceb8b9](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/fceb8b91a3acc9ec98edcd9d296c9355abedda3e))
* Vereinfachung von Neuigkeiten ([eea53ba](https://gitlab.com/forestdesign/mvobermeitingen/website/commit/eea53bab30aeb5ec31562a939c6e73f47c1372be))
