import {defineCollection, reference, z} from 'astro:content';

const blog_category = defineCollection({
  type: 'data',
  schema: z.object({
    name: z.string(),
  }),
});

const blog = defineCollection({
  // Type-check frontmatter using a schema
  schema: z.object({
    title: z.string(),
    description: z.string(),
    category_ref: reference('blog_category'),
    related_konzert_ref: reference('konzert').optional(),
    // Transform string to Date object
    pubDate: z
      .string()
      .or(z.date())
      .transform((val) => new Date(val)),
    updatedDate: z
      .string()
      .optional()
      .transform((str) => (str ? new Date(str) : undefined)),
    heroImage: z.string().optional(),
  }),
});

const instrument_gruppe = defineCollection({
  type: 'data',
  schema: z.object({
    name: z.string(),
    order: z.number()
  }),
});

const instrument = defineCollection({
  type: 'data',
  schema: z.object({
    name: z.string(),
    gruppe_ref: reference('instrument_gruppe')
  }),
});

const musiker = defineCollection({
  type: 'data',
  schema: z.object({
    vorname: z.string(),
    nachname: z.string(),
    instrument_ref: z.array(reference('instrument')).optional(),
    image: z.string().optional()
  }),
});

const vorstand = defineCollection({
  type: 'data',
  schema: z.object({
    rolle: z.string(),
    musiker_ref: reference('musiker'),
    order: z.number(),
  }),
});

const konzert_gruppe = defineCollection({
  type: 'data',
  schema: z.object({
    name: z.string(),
    order: z.number(),
    stamp: z.string()
  })
});

const konzert = defineCollection({
  type: 'content',
  schema: z.object({
    title: z.string(),
    description: z.string(),
    heroImage: z.string(),
    date: z.string()
      .or(z.date())
      .transform((val) => new Date(val)),
    endDate: z.string()
      .or(z.date())
      .transform((val) => new Date(val)),
    gruppe_ref: reference('konzert_gruppe'),
    location: z.string(),
    price: z.number().nullish(),
    veranstalter: z.string(),

    musiker_ref: z.array(reference('musiker')).optional(),
    related_blog_ref: reference('blog').optional(),
    locationUrl: z.string().optional(),
    locationImage: z.string().optional(),
    priceDescription: z.string().optional(),
    programm: z.array(z.object({
      title: z.string(),
      description: z.string(),
      time: z.string()
        .or(z.date())
        .transform((val) => new Date(val)),
      ca: z.boolean(),
      endTime: z.string()
        .or(z.date())
        .transform((val) => new Date(val))
    })).optional(),
    goody: z.array(z.string()).optional(),
    sponsoren: z.array(z.string()).optional(),
    abgesagt: z.boolean().optional(),
    verschoben: z.boolean().optional(),
  })
});


export const collections = {blog, blog_category, instrument, instrument_gruppe, musiker, vorstand, konzert_gruppe, konzert };
