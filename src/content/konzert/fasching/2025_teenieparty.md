---
title: "Teenie Party 2025"
description: "Feiert mit uns die Teenie Party 2025. Wir freuen uns auf euch!"
heroImage: "konzerte/2025/teenieparty.jpg"
date: "2025-02-22T18:00:00+01:00"
endDate: "2025-02-22T22:00:00+01:00"
gruppe_ref: "fasching"
location: 'Musikerheim (Rathaus) Obermeitingen'
price: 3.00
priceDescription: "Karten im Vorverkauf ab Januar 2025 erhältlich"
veranstalter: "Musikverein Obermeitingen e.V."
sponsoren: [ "2023/sponsor_blackbox.svg", "2023/sponsor_gothaer.svg", "2023/sponsor_mayr.svg", "2023/sponsor_strobl.svg" ]
---
