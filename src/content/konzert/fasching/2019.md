---
title: "Kinderfasching 2019"
description: "Der Kinderfasching des Musikvereins Obermeitingen e.V. findet am 24.02.2019 statt. Wir laden euch ein zu einem gemütlichen Nachmittag mit Spiel, Spaß und Tanz."
heroImage: "konzerte/2019/fasching/kinderfasching_2019.jpg"
date: "2019-02-24T13:30:00+01:00"
endDate: "2019-02-24T17:00:00+01:00"
gruppe_ref: "fasching"
location: 'Bürgerhaus Obermeitingen'
price: 4.00
priceDescription: "Kinder unter 2 Jahren frei"
veranstalter: "Musikverein Obermeitingen e.V."
goody: ["DJ", "Minigarde", "viele Spiele", "tolle Preise", "Umzug", "Party"]
programm: [
  { 
    title: 'Kinderfaschingsumzug', 
    description: 'Treffpunkt: 13:15 Uhr am Rathausplatz Obermeitingen', 
    time: "2020-02-16T13:30:00+02:00", 
    endTime: "2020-02-16T14:00:00+02:00", 
    ca: false 
  },
  { 
    title: 'Kinderfaschingsparty', 
    description: 'Mit DJ, Minigarde, vielen Spielen und tollen Preisen. Treffpunkt: Bürgerhaus Obermeitingen', 
    time: "2020-02-16T14:00:00+01:00", 
    endTime: "2020-02-16T17:00:00+01:00", 
    ca: false 
  },
]
related_blog_ref: "2019/vorbericht_fasching_2019"
---
## Kinderfasching mit dem Musikverein

Coole Feuerwehrmänner, Cowboys, Piraten,
Prinzessinnen und viele kleine flauschige Zootiere
aus dem ganzen Lechfeld trafen sich am
Sonntag, den 24.02.2019, wieder zum traditionellen
Kinderfasching des Musikvereins Obermeitingen.

Während des Umzugs durch Obermeitingen,
angeführt von der Jugendkapelle des Musikvereins
unter der Leitung von Wolfgang Forster,
sammelten die vielen Kinder bei bestem
Wetter die Süßigkeiten ein. Dabei wurde der
Zug von Straße zu Straße immer länger.

Im ausverkauften Saal des Bürgerhauses ging
es anschließend rund.

Für den Nachmittag hatten DJ Sascha Ertle,
Georg Weihmayer und Vanessa Waldheim ein
mitreißendes Programm zusammengestellt und
standen als Animateure auf der Bühne.

Die faschingsbegeisterten Kinder und ihre verkleideten
Eltern hatten eine Menge Spaß bei
den zahlreichen Party- und Tanzspielen wie
Polonaise, Luftballon-Tanzen, Limbo-Wettbewerb
und Schaumkuss-Wettessen. Sogar die
ganz Kleinen tanzten an Mamas Hand oder auf
Papas Arm mit.

Auch Preise gab es zu gewinnen und in den Pausen
konnte man sich mit Pommes oder am Kuchenbuffet
stärken.

Viel Applaus gab es für die Minigarde des Mittelstetter
Faschingsclubs für ihre tollen Tänze.
