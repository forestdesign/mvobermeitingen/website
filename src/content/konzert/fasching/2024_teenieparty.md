---
title: "Teenie Party 2024"
description: "Feiert mit uns die Teenie Party 2024. Wir freuen uns auf euch!"
heroImage: "konzerte/2024/teenieparty_2024.jpg"
date: "2024-02-10T18:00:00+01:00"
endDate: "2024-02-10T22:00:00+01:00"
gruppe_ref: "fasching"
location: 'Musikerheim (Rathaus) Obermeitingen'
price: 3.00
veranstalter: "Musikverein Obermeitingen e.V."
sponsoren: [ "2023/sponsor_blackbox.svg", "2023/sponsor_gothaer.svg", "2023/sponsor_mayr.svg", "2023/sponsor_strobl.svg" ]
---
