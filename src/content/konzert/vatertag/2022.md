---
title: Vatertagsfest 2022
description: ""
heroImage: konzerte/2022/vatertag/2022_Vatertagsfest-Plakat.jpg
date: 2022-05-26T09:45:00+02:00
endDate: 2022-05-26T16:45:00+02:00
gruppe_ref: "vatertag"
location: Feststadl Obermeitingen
price: null
veranstalter: Musikverein Obermeitingen e.V.

---

## Gelungenes Vatertagsfest in Obermeitingen mit dem Musikverein

Endlich wieder gemeinsam feiern! Bei herrlichem Wetter
kamen auf dem Spielplatz am Feststadel in Obermeitingen
Jung und Alt zusammen, um gemeinsam den Vatertag
zu feiern.

Nach einem Flurumgang und einem Gottesdienst zum
Fest Christi Himmelfahrt, der vom Kinderchor „Ohrwurm“
und einer Jungbläsergruppe musikalisch gestaltet wurde,
wartete das Mittagessen zum Beispiel mit Schweinebraten,
Holzofenpizza und Bratwürstl auf die Besucher.

Die Mitglieder des Musikvereins Obermeitingen sorgten
nicht nur für das leibliche Wohl – später am Nachmittag
gab es noch Kaffee und Kuchen -, sie hatten auch verschiedene
Attraktionen und Spielstationen für die Kinder
vorbereitet.

So konnten die Kinder sich beim Wettnageln, Eierlauf
oder Torwandschießen messen, Nagelbilder herstellen
oder sich schminken lassen. Bei der anschließenden
Preisverleihung gab es Gutscheine, z.B. vom Bäcker,
der Eisdiele oder zum Pizza- oder Döneressen.

Zur Unterhaltung spielte die Jugendkapelle des Musikvereins
unter der Leitung von Wolfgang Forster. „Ein gelungenes
Fest“, resümierte Michael Weihmayer, der Vorsitzende
des Musikvereins, der überwältigt war, dass so
viele Besucher gekommen waren.


![](/konzerte/2022/vatertag/20220526-1005-DSC06744.jpg)
![](/konzerte/2022/vatertag/20220526-1013-DSC06746.jpg)
![](/konzerte/2022/vatertag/20220526-1026-DSC06758.jpg)
![](/konzerte/2022/vatertag/20220526-1222-DSC06797.jpg)
![](/konzerte/2022/vatertag/20220526-1244-DSC06819.jpg)
![](/konzerte/2022/vatertag/20220526-1323-DSC06840.jpg)
![](/konzerte/2022/vatertag/20220526-1337-DSC06849.jpg)
![](/konzerte/2022/vatertag/20220526-1341-DSC06854.jpg)
![](/konzerte/2022/vatertag/20220526-1401-DSC06859.jpg)
![](/konzerte/2022/vatertag/20220526-1503-DSC06881.jpg)
