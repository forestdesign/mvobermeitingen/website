---
title: "Kirchenkonzert 2025"
description: "Das Kirchenkonzert des Musikvereins Obermeitingen e.V. findet wieder statt. Wir laden euch ein zu einem gemütlichen Abend mit Blasmusik."
heroImage: "konzerte/2025/kirchenkonzert.jpg"
date: "2025-04-13T17:00:00+02:00"
endDate: "2025-04-13T19:00:00+02:00"
gruppe_ref: "kirchenkonzert"
location: 'Wallfahrtskirche Klosterlechfeld'
price: null
priceDescription: "Spenden erwünscht"
veranstalter: "Musikverein Obermeitingen e.V."
goody: []
sponsoren: [ "2023/sponsor_blackbox.svg", "2023/sponsor_gothaer.svg", "2023/sponsor_mayr.svg", "2023/sponsor_strobl.svg" ]
---
