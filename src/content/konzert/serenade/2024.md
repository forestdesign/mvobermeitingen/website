---
title: Serenade 2024
description: Event wird noch geplant
heroImage: konzerte/2024/serenade_2024_plakat.jpg
date: 2024-07-14T18:00:00+01:00
endDate: 2024-07-14T20:00:00+01:00
gruppe_ref: "serenade"
location: 'Spielplatz am Feststadt Obermeitingen'
price: null
priceDescription: Event wird noch geplant
veranstalter: Musikverein Obermeitingen e.V.
sponsoren: [ 2023/sponsor_blackbox.svg, 2023/sponsor_gothaer.svg, 2023/sponsor_mayr.svg, 2023/sponsor_strobl.svg ]
---
