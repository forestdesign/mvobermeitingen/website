---
title: Serenade 2021
description: Die jährliche Serenade des Musikvereins Obermeitingen e.V. findet am 25.07.2021 statt. Wir laden euch ein zu einem gemütlichen Abend mit Blasmusik und Biergartenstimmung.
heroImage: konzerte/2021/serenade/Plakat-Serenade-Rathausplatz.jpg
date: 2021-07-25T19:00:00+02:00
endDate: 2021-07-25T21:00:00+02:00
gruppe_ref: "serenade"
location: 'Rathhausplatz Obermeitingen'
price: null
veranstalter: Musikverein Obermeitingen e.V.
related_blog_ref: 2021/vorbericht_serenade_2021
---

## Serenade auf dem Rathausplatz

Endlich wieder ein Livekonzert! Genau ein Jahr nach
ihrem letzten Auftritt lud der Musikverein Obermeitingen
am 25. Juli 2021 zu einer Serenade auf dem Rathausplatz
ein.

„Seit Ende Juni proben wir wieder - bei trockenem Wetter
im Freien auf dem Rathausplatz, ansonsten im Probenraum
mit reduzierter Truppe“, berichtete Michael Weihmayer,
der Vorsitzende des Musikvereins.

In der kurzen Probenzeit bis zur Serenade hat Robert
Sibich, der seit Januar 2020 das Orchester leitet, mit seinen
Musikern und Musikerinnen drei neue Stücke einstudiert.

Zahlreiche Besucher nahmen auf den Bänken, die auf
der gesperrten Straße Am Kirchberg aufgestellt waren,
Platz und genossen das einstündige Konzert, das sogar
die Abendsonne noch hervorlockte.

Eröffnet wurde der Abend durch die Jugendkapelle des
Musikvereins unter der Leitung von Wolfgang Forster, die
eine beachtliche Darbietung zeigte.

Die talentierten Nachwuchsmusiker starteten mit dem
„Famous Canon“ von Alfred Bösendorfer, der sich von
Pachelbels berühmten Kanon in D-Dur inspirieren ließ.
Mit einem Medley erinnerten sie an Stephen Foster, einem
amerikanischer Songwriter aus dem 19. Jahrhundert,
aus dessen Feder zum Beispiel „Oh Susanna“
stammt. Weiter ging es mit dem fröhlichen „I love Polka“
von Alexander Pfluger. Als Zugabe wählte die Jugendkapelle
den Shanty "Soon May the Wellerman Come", der
Anfang 2021 durch die Version von Nathan Evans weltbekannt
wurde.

Das symphonische Blasorchester mit seinem Dirigenten
Robert Sibich erfreute das Publikum nicht nur mit traditioneller
Blasmusik, wie dem „Castaldo-Marsch“, dem
Marsch „Gruß an Prag“ und der Polka „Böhmischer
Traum“.

Die Musiker begeisterten auch mit
einem Medley aus Beatles-Hits, bei
dem viele Zuschauer im Takt mitwippten.
Bei diesem rhythmisch
anspruchsvollen Stück mit seinen
raschen Tempowechseln zeigte
das Orchester quer durch alle Register,
dass es in der Coronapause
nichts von seinem hohen musikalischen
Niveau eingebüßt hat.

Davon zeugte auch das mitreißende
Medley lateinamerikanischer
Melodien. Vor allem das Percussionensemble
war groß in Form, Sascha
Ertle glänzte mit einem
Schlagzeugsolo.

Mit der Polka „Böhmische Liebe“ beendete der Musikverein das mit viel Disziplin
einstudierte und viel Spielfreude präsentierte Programm, durch das Heidi Weihmayer
charmant führte.

Im Rahmen der Serenade zeichnete der Musikverein eine Musikerin der Jugendkapelle
aus: Die 14-jährige Oboistin Constanze Lichtblau bestand die Musikerleistungsprüfung
D1 „mit sehr gutem Erfolg“ und erhielt das Musikerleistungsabzeichen
Bronze des Musikbundes von Ober- und Niederbayern e.V..


![](/konzerte/2021/serenade/20210725-1820-DSC06463.jpg)
![](/konzerte/2021/serenade/20210725_184935.jpg)
![](/konzerte/2021/serenade/20210725_190107.jpg)
![](/konzerte/2021/serenade/20210725_192052.jpg)
![](/konzerte/2021/serenade/20210725_200154.jpg)
