---
title: "In Konzert 2025"
description: "Feiert mit uns das neue Jahr mit unserem Auftaktkonzert 2025. Wir freuen uns auf euch! Für euch spielen sowohl die Jugendkapelle unter der Leitung von Karina Schönberger als auch das Blasorchester unter der Leitung von Robert Sibich."
heroImage: "konzerte/2025/in_konzert_2025.jpg"
date: "2025-01-25T19:30:00+01:00"
endDate: 2025-01-25T19:30:00+01:00
gruppe_ref: "in_konzert"
location: 'Bürgerhaus Obermeitingen'
price: 9.00
priceDescription: "Karten nur über die Abendkasse erhältlich"
veranstalter: "Musikverein Obermeitingen e.V."
sponsoren: [ "2023/sponsor_blackbox.svg", "2023/sponsor_gothaer.svg", "2023/sponsor_mayr.svg", "2023/sponsor_strobl.svg" ]
related_blog_ref: 2024/ankuendigung_in_konzert
---
