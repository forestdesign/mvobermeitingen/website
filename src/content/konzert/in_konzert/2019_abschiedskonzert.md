---
title: "In Konzert 2019 - Abschiedskonzert"
description: ""
heroImage: "konzerte/2019/konzert/20191207-2125-DSC06109.jpg"
date: "2019-12-07T19:30:00+01:00"
endDate: "2019-12-07T21:00:00+01:00"
gruppe_ref: "in_konzert"
location: 'Bürgerhaus Obermeitingen'
price: null
veranstalter: "Musikverein Obermeitingen e.V."
---

## Live in Konzert am 07.12.2019

Bis auf den letzten Platz besetzt war der Saal des Obermeitinger
Bürgerhauses beim letzten Konzert des
Blasorchesters des Musikvereins unter der Leitung von
Daniela Rid.

„Dankeschön Daniela“, dies war das Motto des Abends,
das nicht nur in rührigen Worten Ausdruck fand, sondern
am Schluss auch als Banner hinter der Bühne zu sehen
war.

Eine Auswahl der schönsten Stücke der vergangenen
neun Jahre, in denen Daniela Rid die Leitung innehatte,
wartete auf die Besucher. Und der Abend startete fulminant
mit der „Jupiter Hymn“.

Die beiden Moderatoren Dominik Lauter und Georg
Weihmayer navigierten als Ballonfahrer das Publikum
durch das abwechslungsreiche Programm.

Bei den Operettenklängen aus der „Leichten Kavallerie“
von Franz von Suppé zeigte das Orchester, was es
drauf hat. Auf dem guten Fundament des tiefen Blechs
brillierten die Klarinetten und Flöten mit schnellen Läufen,
Trompeten und Xylophon schufen den Trab der
Pferde. Die Zuhörer dankten es mit Bravorufen.

Fesseln ließ sich das Publikum von „At the break of
Gondwana“ des singapurischen Komponisten Benjamin
Yeo. Hier zog Daniela Rid tatsächlich noch mal alle Register.
Vor allem das sechsköpfige Percussion-Ensemble meisterte seine Herausforderungen grandios.

Nach Ernst Uebels Militärmarsch „Jubelklänge“ ging
es weiter nach Paris. Mit „Paris Montmartre“, einem
Medley bekannter französischen Melodien, fühlte
man sich gleich mitten in der Metropole an der Seine.

Im Foyer vor dem Saal hatten die Besucher während
der Pause Gelegenheit, die letzten Jahre des Orchesters
unter Daniela Rids Leitung Revue passieren
zu lassen. Dafür hatten die Musiker zahlreiche
Fotos, Programme und Zeitungsartikel aufgehängt.

Im zweiten Teil des Konzerts ging es „Around the
World in 80 days“ und zurück in die 1920er-Jahre
mit Musik aus „The crazy Charleston Era“.

Viel Freude hatten Musiker und Zuhörer mit der actionreichen
Filmmusik aus den Indiana-Jones-Filmen.

Die Lieder aus Ralf Benatzkys Singspiel „ Im weißen
Rößl“ rissen am Ende des Konzerts die Besucher
mit.

Aber es wurde auch emotional, als die Moderatoren
im Namen aller Musiker ihren Dank „für neun harmonische
und erfolgreiche Jahre“ zum Ausdruck brachten
mit den Worten: „Wir verneigen uns vor Dir.“ Und
natürlich ergriff auch Michael Weihmayer als erster
Vorstand des Musikvereins das Wort und dankte
Daniela Rid mit einem großen Blumenstrauß.

Am Schluss ließ es sich auch Bürgermeister Erwin
Losert nicht nehmen, Daniela Rid im Namen der Gemeinde
zu danken für „ihr Engagement und ihr Herzblut
und die vielen schönen Stunden, in denen sie
uns mit wunderbarer Blasmusik verwöhnt hat“.

„Dankeschön auch dafür, dass du mit dem Auftritten des
Musikvereins die Gemeinde Obermeitingen weit über die
Gemeindegrenzen hinaus positiv bekannt gemacht hast.“

Als aktive Musikerin wird Daniela Rid dem Orchester
erhalten bleiben.

Nach dem Konzert bedankten sich die Musiker bei einer
„After-Show-Party“ noch mit Einlagen sowie kleinen Geschenken
bei Daniela Rid und verabschiedeten sie als
Dirigentin.

![](/konzerte/2019/konzert/20191207-2116-DSC06102.jpg)
![](/konzerte/2019/konzert/20191207-2125-DSC06109.jpg)
![](/konzerte/2019/konzert/20191207-2132-DSC06116.jpg)
![](/konzerte/2019/konzert/20191207-2235-DSC06131.jpg)
![](/konzerte/2019/konzert/20191207-2255-DSC06142.jpg)
