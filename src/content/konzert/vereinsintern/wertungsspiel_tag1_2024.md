---
title: "Wertungsspiele (Tag 1)"
description: "Ein Vereinsinternes Event des Musikvereins Obermeitingen e.V. findet am 17.09.2023 statt. Wir laden alle Mitglieder ein zu einer Radtour entlang des Lechs."
heroImage: "konzerte/2024/wertungsspiele_2024.jpg"
date: "2024-04-20"
endDate: "2024-04-20"
gruppe_ref: "vereinsintern"
price: null
location: ""
priceDescription: "Nur für Vereinsmitglieder"
veranstalter: "Musikverein Obermeitingen e.V."
sponsoren: [ "2023/sponsor_blackbox.svg", "2023/sponsor_gothaer.svg", "2023/sponsor_mayr.svg", "2023/sponsor_strobl.svg" ]
---
