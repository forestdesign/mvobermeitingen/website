---
title: "Tour de Lech"
description: "Ein Vereinsinternes Event des Musikvereins Obermeitingen e.V. findet am 17.09.2023 statt. Wir laden alle Mitglieder ein zu einer Radtour entlang des Lechs."
heroImage: "konzerte/2023/tour_de_lech.jpg"
date: "2023-09-17T09:45:00+02:00"
endDate: "2023-09-17T16:00:00+02:00"
gruppe_ref: "vereinsintern"
price: null
location: "Baggersee Obermeitingen"
priceDescription: "Nur für Vereinsmitglieder"
veranstalter: "Musikverein Obermeitingen e.V."
sponsoren: [ "2023/sponsor_blackbox.svg", "2023/sponsor_gothaer.svg", "2023/sponsor_mayr.svg", "2023/sponsor_strobl.svg" ]
---

Nach längerer Zeit veranstaltete der Musikverein Obermeitingen wieder einen Radlausflug für ihre Musiker und Familien. Am Morgen des 17. September war es endlich so weit. Alle haben sich bei sonnigem Wetter mit ihren Fahrrädern am Feststadl getroffen.

Jetzt ging es endlich los, auf 3 unterschiedlichen Touren haben sich alle auf den Weg gemacht. Egal ob im kleinen Radius mit den Kindern bis zur Lechstaustufe, oder auf den größeren Touren bis nach Beuerbach.

Am gemeinsamen Ende der anstrengenden Radtouren trafen sich alle wieder am Feststadl und stärkten sich am Hamburger-Buffett.

Am Nachmittag startete man in Kleingruppen in einen kleinen Spielewettkampf. Viele lustige Spiele wie Wasserschöpfen, Sackhüpfen oder Wäsche aufhängen bei wunderbarem Wetter sorgten für viel gute Laune. Der Tag endete schließlich mit einer Siegerehrung mit Preisen für alle Mitspieler.


![](/konzerte/2023/radlausflug/20230917-1307-DSC08592.JPG)
![](/konzerte/2023/radlausflug/20230917-1428-DSC08598.JPG)
![](/konzerte/2023/radlausflug/20230917-1435-DSC08602.JPG)
![](/konzerte/2023/radlausflug/20230917-1445-DSC08607.JPG)
![](/konzerte/2023/radlausflug/20230917-1448-DSC08609.JPG)
![](/konzerte/2023/radlausflug/20230917-1512-DSC08613.JPG)
![](/konzerte/2023/radlausflug/20230917-100752(0).jpg)
![](/konzerte/2023/radlausflug/20230917-160554.jpg)
![](/konzerte/2023/radlausflug/20230917-161001.jpg)
![](/konzerte/2023/radlausflug/20230917-161054.jpg)
![](/konzerte/2023/radlausflug/20230917-161139(0).jpg)
![](/konzerte/2023/radlausflug/20230917-161207(0).jpg)
![](/konzerte/2023/radlausflug/20230917-161257(0).jpg)
![](/konzerte/2023/radlausflug/20230917-161319(0).jpg)
![](/konzerte/2023/radlausflug/20230917-161342.jpg)
![](/konzerte/2023/radlausflug/20230917-161442.jpg)
![](/konzerte/2023/radlausflug/20230917-161538(0).jpg)
![](/konzerte/2023/radlausflug/20230917-161551.jpg)
