---
title: "Casino Night + Weihnachtsfeier"
description: "Ein Vereinsinternes Event des Musikvereins Obermeitingen e.V. findet am 18.11.2023 statt. Wir laden alle Mitglieder und Sponsoren ein zu einem Casinoabend im Vereinsheim. Nur in Abendgardarobe."
heroImage: "konzerte/2023/casinoabend_2023.jpg"
date: "2023-12-09T19:00:00+01:00"
endDate: "2023-12-09T24:00:00+01:00"
gruppe_ref: "vereinsintern"
price: null
location: "Musikerheim Obermeitingen"
priceDescription: "Nur für Vereinsmitglieder und Sponsoren"
veranstalter: "Musikverein Obermeitingen e.V."
sponsoren: [ "2023/sponsor_blackbox.svg", "2023/sponsor_gothaer.svg", "2023/sponsor_mayr.svg", "2023/sponsor_strobl.svg" ]
verschoben: false
---
