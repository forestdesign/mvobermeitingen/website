---
title: "Jugenddirigent verabschiedet sich nach zwölf Jahren"
description: "Jugenddirigent verabschiedet sich nach zwölf Jahren"
category_ref: "allgemein"
pubDate: "2023-07-27T00:00:00+02:00"
heroImage: "/konzerte/2023/serenade/wolfgang_forster_12_jahre_abschied.jpg"
related_konzert_ref: "serenade/2023"
---
Wolfgang Forster aus Langerringen ist seit 12 Jahren der Dirigent des Jugendblasorchesters in Obermeitingen. Jahr für Jahr spielen in der wöchentlichen Musikprobe konstant 25 Jungmusiker unter seiner humorvollen und fachlichen Leitung.

Mit seiner positiven Art und viel Hingabe zur Musik schafft er es, die Kinder und Jugendlichen zum regelmäßigen Probenbesuch zu motivieren.

Er sorgt gerne für ein bunt gemischtes Repertoire bestehend aus Märschen, Polkas, konzertanten Stücken und angesagten Hits.

Die vergangenen zwölf Jahre waren gefüllt mit musikalischen Höhepunkten wie die wiederkehrenden Jahreskonzerte, an denen die jungen Nachwuchsmusiker unter Forsters bewährter Leitung immer für ein tolles Programm gesorgt haben. Serenaden am See, Unterhaltungsmusik am Vatertagsfest, Weihnachtsmärkte, Feuerwehrfeste und auch einmalige Aktionen wie die musikalische Umrahmung zum 60-jährigen Bestehen der Wasserwacht Schwabmünchen waren weitere Höhepunkte. Auch bei Wertungsspielen haben Forster und die jungen Musiker jährlich gemeinsam Ihr Können unter Beweis gestellt und sich Auszeichnungen erarbeitet.

Mit seinen Musikern hat er gerne seine Zeit verbracht. Egal, ob bei Radtouren, auf Probenwochenenden in Dinkelscherben, Bezau, am Ammersee oder bei Ausflügen in den Kletterwald und Schlittenfahrt in Immenstadt war er immer mit dabei und hatte mit den Musikern seinen Spaß.

Daran konnte man deutlich erkennen, dass dies bei ihm nicht nur „Arbeit“ war, sondern eine Herzensangelegenheit, welche den Abschied umso schmerzlicher macht.

