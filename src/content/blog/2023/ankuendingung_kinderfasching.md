---
title: "Faschingscountdown und -umzug am 12. Februar in Obermeitingen"
description: "Faschingscountdown und -umzug am 12. Februar in Obermeitingen"
category_ref: "fasching"
pubDate: "2023-02-08T00:00:00+01:00"
heroImage: "/konzerte/2023/fasching_2023.jpg"
related_konzert_ref: "fasching/2023"
---
Am 12. Februar veranstaltet der Musikverein Obermeitingen nach der Corona-Zwangspause endlich wieder einen Kinderfasching.
Mit einem Faschings-Countdown werden wir den Faschingstag in Obermeitingen einläuten: Am Feststadl geht es um 12 Uhr los mit kleinen Konzerten von der Jugendkapelle Obermeitingen, den Trommelgruppen des Musikvereins Obermeitingen und den Sambatrommeln der Musikschule Untermeitingen. Kommen Sie zum Feststadl Obermeitingen und genießen sie neben den musikalischen Darbietungen auch unsere leckeren Pommes und Leberkässemmeln.

Ab 13.30 Uhr startet dann der Umzug durch Obermeitingen mit Garde, Sambatrommeln, begleitet von den Musikern der Jugendkapelle. Wir freuen uns über jeden, der mitläuft oder Süßigkeiten wirft. Der Umzug endet am Rathausplatz, von dort geht es direkt zum Kinderfasching im Bürgerhaus.

Der Musikverein Obermeitingen lädt Sie herzlich zum Faschingscountdown und Umzug ein.

Vanessa Waldheim
