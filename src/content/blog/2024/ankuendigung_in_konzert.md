---
title: "In Konzert am 27. und 28. Januar"
description: "Am 27. und 28. Januar 2024 veranstaltet der Musikverein Obermeitingen sein alljährliches Jahreskonzert im Bürgerhaussaal Obermeitingen."
category_ref: "in_konzert"
pubDate: "2024-01-12T00:00:00+02:00"
heroImage: "/konzerte/2024/in_konzert_2024.jpg"
related_konzert_ref: "in_konzert/2024"
---
Seit September hat die Jugendkapelle eine neue Leitung, Karina Schönberger, nach dem der langjährige Dirigent Wolfgang Forster sich im Sommer nach 12 Jahren verabschiedet hat.

Sie hat die Jugendkapelle übernommen und eine neue Jugendgruppe „Die Bambinis“ gestartet. Jeden Montag treffen sich die 15 jungen Musiker zwischen 17.00 und 17.45 Uhr zur Probe. Die neue Gruppe dient als Vorstufe zur Jugendkapelle und nimmt Musiker ab der 5. Klasse auf.

Am Samstagabend, den 27. Januar ab 19.30 Uhr präsentiert sich das Blasorchester unter der Leitung von Robert Sibich und die Jugendkapelle, erstmals unter der Leitung von Karina Schönberger.

Tauchen Sie mit uns in die Welt der Musik. Lauschen Sie den Klängen der Drachen bei „Dragonfight“ oder auch dem Morgenerwachen mit „Morning Star Variations“. Die Reise führt Sie bis nach Washington mit „Washington Post“ und noch weiter. Lassen Sie sich überraschen. Das Blasorchester hat sich an einem Probentag intensiv auf diesen Abend vorbereitet und in den einzelnen Registern die einzelnen Stücke geprobt.

Am Sonntag, dem 28. Januar ab 15 Uhr präsentiert sich der Musikverein mit all seinen Gruppen.

Lassen Sie sich überraschen und genießen Sie bei Kaffee und Kuchen, wie die Bambinis klingen und später auch die Jugendkapelle ihr Bestes zeigt. Als Abschluss gibt es noch einen Auszug des Konzertprogrammes vom Blasorchester präsentiert.

Eintrittskarten für Samstagabend gibt es wie immer an der Abendkasse, die Veranstaltung am Sonntag ist frei.


![1](/neuigkeiten/2024/20230918-1814-DSC08628.jpg)
![1](/neuigkeiten/2024/20231118-1507-DSC08797.jpg)
![1](/neuigkeiten/2024/20231118-1530-DSC04285.jpg)
![1](/neuigkeiten/2024/20231118-1534-DSC04292.jpg)
![1](/neuigkeiten/2024/20231118-1542-DSC04304.jpg)
![1](/neuigkeiten/2024/20231118-1546-DSC04314.jpg)
![1](/neuigkeiten/2024/20231118-1546-DSC04315.jpg)
![1](/neuigkeiten/2024/20231118-1602-DSC04317.jpg)

Wir freuen uns sehr auf Ihren Besuch.

Ihr Musikverein Obermeitingen

Vanessa Waldheim
