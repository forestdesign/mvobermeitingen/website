---
title: "So klingt der Sommer"
description: "45. Bezirksmusikfest und 30. Gründungsjubiläum der Musikkapelle Obermeitingen"
category_ref: "allgemein"
pubDate: "2016-08-01T00:00:00+02:00"
heroImage: "konzerte/2016/bezirksmusikfest/Flyer_BZMF-Vorderseite.jpg"
---
Liebe Obermeitinger,

am Freitag, den 12. August eröffnen wir unser Fest um 18:00 Uhr mit einem Sternmarsch. Aus allen vier Himmelsrichtungen werden acht Musikkapellen musizierend an der Kreuzung Lechfelder Straße / Amselweg eintreffen. Dort angekommen wird es für alle Zuhörer ein gemeinsames Standkonzert geben. Nach der offiziellen Eröffnung des Festes, sind alle eingeladen gemeinsam mit den Musikkapellen ins Festzelt einzuziehen, um dort zu feiern.

Höhepunkt unseres Festes bildet am Sonntag, den 14. August Gemeinschaftschor und Festumzug.

Als Auftakt zum Festumzug begrüßen wir um 13:30 Uhr an der Kreuzung Lechfelder Straße / Amselweg ca. 25 Musikkapellen zu einem Gemeinschaftschor. Anschließend stellen sich alle Musik – und Fußgruppen im Amselweg zum großen Festumzug auf.

Der Umzug beginnt um 14.00 und führt durch folgende Straßen: Amselweg, Dornbuschweg, Hauptstraße, Lechfelder Straße und endet im Festzelt.

Wir laden alle Anwohner und Obermeitinger herzlich ein, mit Freunden und Bekannten dem Festumzug beizuwohnen und durch dekoratives Straßenbild und viel Applaus zum Gelingen des Festes beizutragen. Zur Dekoration der Umzugsroute werden wir zu gegebener Zeit Fähnchen, Festzeichen und eine Festschrift verteilen. Wir bitten die Anwohner der Umzugsroute, ihre Autos am Festsonntag nicht an der Straße zu parken.

Für eventuelle Ruhestörungen bei den Anliegern der Umzugsroute und in Festzeltnähe bitten wir um Verständnis. Wir bedanken uns schon jetzt für jede Art von Unterstützung und hoffen, dass Ihr gemeinsam mit uns dieses Fest feiert!
