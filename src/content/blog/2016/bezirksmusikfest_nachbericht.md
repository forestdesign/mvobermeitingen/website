---
title: "Bezirksmusikfest - eine gelungene Veranstaltung"
description: "Über 25 Blaskapellen, ein abwechslungsreiches Programm und nicht zuletzt bestes Sommerwetter lockten zahlreiche Besucher von Nah und Fern zum viertägigen Bezirksmusikfest des Bezirks Lech-Ammersee im Musikbund von Ober- und Niederbayern nach Obermeitingen. Dank der vorbildlichen Organisation des Musikvereins Obermeitingen, der mit dem Fest auch sein 30-jähriges Bestehen feierte, einer großen Anzahl an Sponsoren und vielen, vielen Helfern war es eine rundum gelungene Veranstaltung."
category_ref: "allgemein"
pubDate: "2016-08-16T00:00:00+02:00"
heroImage: "konzerte/2016/bezirksmusikfest/BZMF-Programm.jpg"
---
Über 25 Blaskapellen, ein abwechslungsreiches Pro-
gramm und nicht zuletzt bestes Sommerwetter lockten
zahlreiche Besucher von Nah und Fern zum viertägigen
Bezirksmusikfest des Bezirks Lech-Ammersee im Musik-
bund von Ober- und Niederbayern nach Obermeitingen.
Dank der vorbildlichen Organisation des Musikvereins
Obermeitingen, der mit dem Fest auch sein 30-jähriges
Bestehen feierte, einer großen Anzahl an Sponsoren
und vielen, vielen Helfern war es eine rundum gelunge-
ne Veranstaltung.

Am Freitag brachten 15 Busse Senioren aus dem ge-
samten Landkreis Landsberg nach Obermeitingen ins
Festzelt zu einem geselligen Nachmittag mit Essen und
zünftiger Blasmusik. Die neu gegründete Ü50-Kapelle
des Bezirks Lech-Ammersee unter der Leitung von An-
dreas Kößler gab bei ihrem ersten Auftritt bekannte Pol-
kas und Märsche zum Besten.

Am Abend läuteten Böllerschüsse den Sternmarsch ein.
Aus allen vier Himmelsrichtungen marschierten je zwei
Blaskapellen mit Pauken und Trompeten unter großem
Applaus der Zuschauer zur Kreuzung Lechfelder Straße/
Amselweg.

Die Musikvereine Weicht, Langerringen, Graben, Unter-
meitingen mit Klosterlechfeld, die Blaskapelle Pritt-
riching, die Trachtenkapelle Scheuring und die Musikka-
pellen aus Gennach und Holzhausen spielten anschlie-
ßend gemeinsam vier Musikstücke, abwechselnd diri-
giert von Daniela Rid und Bezirksjugendleiter Gerhard
Böck.

Michael Weihmayer, der Vorsitzende des Musikvereins
Obermeitingen, entzündete eine Fackel und eröffnete
damit offiziell das Bezirksmusikfest.

Mit böhmisch-mährischen Klängen der Kapelle „Blech &
Co“ unter der Leitung von Toni Müller klang der erste
Abend im weiß-blau geschmückten Festzelt aus.

Auch am Sonntag und Montag spielten den ganzen Tag
Blaskapellen im Zelt auf und unterhielten die Gäste zum
Frühschoppen, zum Mittagessen und bei Kaffee und
Kuchen. An den Abenden sorgten Blasmusik-Bands für
beste Stimmung und ließen die Besucher auf den Bän-
ken und vor der Bühne tanzen, wie bei der Sommer-
nachtsparty mit der mitreißenden Partymusik der Allgäu-
er Formation „d’Muckasäck“.

Am Sonntag zelebrierte Pfarrer Thomas Demel einen
Zeltgottesdienst, der vom Obermeitinger Singkreis ge-
meinsam mit dem Chor „Wir“ aus Hurlach und einem
Instrumentalensemble musikalisch gestaltet wurde.

Vor dem Festzug am Mittag stellten sich die Musiker von
rund 25 Blaskapellen nach Registern zu einem Gemein-
schaftschor auf. Ein tolles Bild, ein toller Klang!

Höhepunkt der Veranstaltung war der Festumzug, an
dem als Ehrengäste auch der Landrat des Landkreises
Landsberg, Thomas Eichinger, und MdL Thomas Goppel
teilnahmen.

Rund 50 Vereine und Blaskapellen aus Obermeitingen,
den Nachbargemeinden und dem gesamten Bezirk Lech-
Ammersee sowie aus St. Willibald in Österreich präsen-
tierten sich mit Fußgruppen und geschmückten Wagen
sowie zu Pferd oder auf dem Radl den zahlreichen Zu-
schauern, die die Straßen säumten.

Der Feiertag Maria Himmelfahrt stand ganz im Zeichen
der Kinder. An der großen Piratenparty nahmen 180 Kin-
der teil. An 15 Stationen konnten sie zum Beispiel im
Sand nach einem Schatz buddeln, trommeln, sich
schminken lassen oder Schmuck und Piratenhüte bas-
teln und Holzsäbel herstellen. Auch eine Torwand,
Kutschfahrten und eine Hüpfburg hatte der Musikverein
für die Kleinen vorbereitet. Die Großen konnten sich der-
weil im Festzelt von den „Musikvagabunden“ mit böh-
misch-mährischer Blasmusik unterhalten lassen.

### Aufbau
![](/konzerte/2016/bezirksmusikfest/tag1/20160806-0702-IMG_0619.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160806-1154-IMG_0680.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160806-1353-IMG_0699.JPG)

### Tag 1
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-1357-IMG_4377.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-1359-IMG_4381.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-1726-DSC02768.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-1736-DSC02777.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-1736-DSC02779.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-1828-IMG_4531.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-1841-IMG_4603.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-1852-IMG_4662.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-2058-IMG_4750.JPG)
![](/konzerte/2016/bezirksmusikfest/tag1/20160812-2332-IMG_4768.JPG)

### Tag 2
![](/konzerte/2016/bezirksmusikfest/tag2/20160813-1948-DSC02829.JPG)
![](/konzerte/2016/bezirksmusikfest/tag2/20160813-1948-DSC02830.JPG)
![](/konzerte/2016/bezirksmusikfest/tag2/20160813-1948-DSC02831.JPG)
![](/konzerte/2016/bezirksmusikfest/tag2/20160813-1949-DSC02834.JPG)
![](/konzerte/2016/bezirksmusikfest/tag2/20160813-2050-DSC02847.JPG)
![](/konzerte/2016/bezirksmusikfest/tag2/20160813-2054-IMG_4854.JPG)
![](/konzerte/2016/bezirksmusikfest/tag2/20160813-2203-IMG_4940.JPG)
![](/konzerte/2016/bezirksmusikfest/tag2/20160813-2204-IMG_4944.JPG)

### Tag 3
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-0026-IMG_5004.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1402-IMG_5104.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1403-IMG_5108.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1413-IMG_5128.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1421-IMG_5143.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1547-IMG_5481.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1548-IMG_5490.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1549-IMG_5504.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1557-IMG_5554.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1557-IMG_5557.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1615-IMG_5591.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-1718-IMG_5643.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-2303-IMG_5675.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-2306-IMG_5686.JPG)
![](/konzerte/2016/bezirksmusikfest/tag3/20160814-2311-IMG_5696.JPG)

### Tag 4
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1308-IMG_5817.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1311-IMG_5825.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1313-IMG_5830.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1318-IMG_5835.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1323-IMG_5843.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1418-DSC02858.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1431-DSC02866.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1553-IMG_5945.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1742-DSC02894.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1814-DSC02900.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1855-IMG_5976.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1923-IMG_6009.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1928-IMG_6027.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1929-IMG_6030.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1939-DSC02902.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1941-DSC02907.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1943-DSC02913.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1944-DSC02921.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-1944-DSC02924.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-2003-IMG_6036.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-2015-DSC02943.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-2111-IMG_6101.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-2111-IMG_6103.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-2235-IMG_6265.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-2340-IMG_6370.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160815-2347-IMG_6425.JPG)
![](/konzerte/2016/bezirksmusikfest/tag4/20160816-1315-IMG_6503.JPG)
