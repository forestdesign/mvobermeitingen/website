---
title: "Große Ereignisse werfen ihre Schatten voraus"
description: "„Der Countdown läuft“ für das Bezirksmusikfest des Musikbundes von Ober- und Niederbayern (MON) im Bezirk Lech-Ammersee. Von Freitag, 12. August, bis Montag, 15. August, veranstaltet der Musikverein Obermeitingen das Großereignis unter dem Motto „So klingt der Sommer“ mit einem vollen Programm im Ort und im Festzelt an der Lechfelder Straße."
category_ref: "allgemein"
pubDate: "2016-08-07T00:00:00+02:00"
heroImage: "konzerte/2016/bezirksmusikfest/Plakat_mit_Programm.jpg"
---
Jetzt wird fleißig die Werbetrommel gerührt. „Wir hängen
Plakate im Umkreis von 25 Kilometern und verteilen Fly-
er und Bierdeckel“, sagt zweiter Vorstand Helmut Knie,
der sich um die Öffentlichkeitsarbeit kümmert. An den
Ortseingängen und am Rathaus weisen schon seit län-
gerem große Banner auf die viertägige Veranstaltung
hin. Mitglieder des Musikvereins werden Festzeichen und
die Festschrift verkaufen.

Seit eineinhalb Jahren steckt der Musikverein, der heuer
sein 30-jähriges Bestehen feiert, in den Vorbereitungen.
Dazu haben sich zehn Mitglieder zu einem Organisati-
onsteam, dem Festausschuss zusammengefunden und
eine Checkliste erstellt mit 360 Punkten, die alle abgear-
beitet werden wollen.

Sei es der Seniorennachmittag, der Sternmarsch oder
der Familientag – jede einzelne Veranstaltung muss bis
ins Detail geplant werden. Auch um die Technik, das Es-
sen und den Ausschank müssen sich die Organisatoren
kümmern, denn der Verein übernimmt die Bewirtung im
Festzelt in Eigenregie. Um Stromspitzen aufzufangen,
vor allem beim Küchenbetrieb im Zelt, müssen Stromag-
gregate besorgt werden.

„Viel Arbeit macht das Zelt“, betont Georg Weihmayer.
„Letzte Details gilt es noch zu klären, damit das Zelt am
ersten Augustwochenende aufgebaut werden kann.“
Schmuckstück der Innenausstattung soll die maßange-
fertigte Cocktailbar werden.

Mitglieder des Festausschusses waren im vergangenen
Jahr auf vielen Festen in der näheren und weiteren Um-
gebung, um herausragende Kapellen und Bands für ihr
Bezirksmusikfest zu finden. Jetzt können sich die Besu-
cher auf „Blech & Co“., „d’Muckasäck“, die Blasband
„Ansatzlos“ und „Die Hurlacher“ freuen, die an den Aben-
den im Festzelt bestimmt für beste Stimmung sorgen
werden.

„Highlights des Bezirksmusikfestes werden die Sommer-
nachtsparty am Samstag und der Festumzug“, verspricht
Michael Weihmayer, der Vereinsvorstand. 50 Gruppen,
darunter zahlreiche örtliche Vereine, nehmen am Fest-
umzug am Sonntagnachmittag durch die festlich ge-
schmückten Straßen des Ortes teil.

Der Musikverein hat rund 50 Firmen aus Obermeitingen
und dem Umland als Sponsoren gewinnen können, die
das Bezirksmusikfest finanziell und mit Equipment unter-
stützen.

„Wir freuen uns auch über die vielen freiwilligen Helfer
aus der Obermeitinger Bevölkerung“, sagt Altbürger-
meister und dritter Vorstand Clemens Weihmayer. Den-
noch werden weitere Helfer und Kuchenbäcker gesucht.
Der Verein erwartet 4000 bis 5000 Besucher.
