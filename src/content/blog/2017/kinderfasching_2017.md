---
title: "Kinderfasching am 26. Februar"
description: "Für die faschingsbegeisterten Kinder aus Obermeitingen und Umgebung hieß es am Sonntag, den 26.Februar 2017 wieder: Partystimmung! Dieses Jahr war das Programm unter dem Motto „Halli Galli-Zirkus“ gestellt."
category_ref: "fasching"
pubDate: "2017-02-26T00:00:00+02:00"
heroImage: "konzerte/2017/fasching/20170226-1520-DSCN8626.jpg"
---
Begonnen hat der Nachmittag mit einem etwa halbstündigen Umzug durchs Dorf mit der Jugendkapelle Obermeitingen unter der Leitung von Wolfgang Forster und der Kindergarde des Mittelstetter Faschingsclubs (MFC). Die Kinder sammelten meistens mit ihren Eltern oder Großeltern viele Süßigkeiten ein, mit denen die Anwohner am Weg die Kinder erfreuten

Im Saal angekommen gab es zuerst eine Einlage der Minigarde und der Kindergarde des MFC. Zum Abschluss brachten sie noch einen Showtanz zu „völlig unverfroren“ auf die Bühne.

Ob bei Polonaisen, Spielen oder Tänzen – die Kinder standen immer im Mittelpunkt und die Mitglieder des Musikvereins Obermeitingen taten alles, damit die Kleinen einen ausgelassenen Nachmittag im Bürgerhaussaal hatten.

Auf der Bühne begeisterten Zirkusdirektor Georg Weihmayer, Clown Vanessa Waldheim und DJ Sascha die Kinder für ihr Unterhaltungsprogramm. Und die kleinen Prinzessinnen, Piraten, Polizisten und Indianer machten gerne mit, die ganz kleinen auf dem Arm von Mama oder Papa.

In zwei Spielrunden konnten die Kinder einen größeren Preis gewinnen. Und wenn sie mal nicht auf der Tanzfläche waren, so stärkten sie sich mit Pommes oder Kuchen.

Wieder einmal wurde der Saal zu einer Kinderdisco verwandelt.

![](/konzerte/2017/fasching/20170226-1358-DSCN8295.jpg)
![](/konzerte/2017/fasching/20170226-1416-DSCN8353.jpg)
![](/konzerte/2017/fasching/20170226-1424-DSCN8400.jpg)
![](/konzerte/2017/fasching/20170226-1429-DSCN8430.jpg)
![](/konzerte/2017/fasching/20170226-1515-DSCN8612.jpg)
![](/konzerte/2017/fasching/20170226-1520-DSCN8626.jpg)
![](/konzerte/2017/fasching/20170226-1551-DSCN8657.jpg)
![](/konzerte/2017/fasching/20170226-1633-DSCN8694.jpg)
