---
title: "Benefizveranstaltung am 23.07.2017: Ein Fest für Claudia"
description: "Ganz im Zeichen der Gemeinschaft, der Geselligkeit und des guten Zwecks stand das vergangene Wochenende in Obermeitingen. Und vor allem auch im Zeichen der Musik. Das Wochenende startete mit der alljährlichen Serenade des Gesangvereins „Fröhlichkeit“ im Feststadel (siehe Bericht)."
category_ref: "serenade"
pubDate: "2017-07-23T00:00:00+02:00"
heroImage: "konzerte/2017/serenade/20170723-1139-DSC03982.jpg"

---
Bei der ganztägigen Veranstaltung „Ein Fest für Claudia“
war am Sonntag fast die ganze Gemeinde auf den Beinen.
Mit vielen fleißigen Helfern richtete die Obermeitinger
Dorfgemeinschaft dieses Benefizfest für ihre schwer
erkrankte Mitbürgerin Claudia Jakob und ihre Familie
aus, die auch zum Fest gekommen waren.

Die 28-Jährige erlitt nach einer Hirnoperation im September
vergangenen Jahres einen Schlaganfall, der ihre
linke Hirnhälfte zerstörte. „Entgegen aller ärztlicher Meinungen
hat sie überlebt und gekämpft“, erzählte ihre
Mutter Petra Jakob. Ihre Tochter lag drei Monate im
Wachkoma und war vier Monate in Rehabilitation. Seit
Ende Februar lebt sie wieder - gemeinsam mit ihrem
neunjährigen Sohn Philipp - in ihrem Elternhaus, das
Petra und Hermann Jakob mit hohem finanziellem Aufwand
behindertengerecht umgebaut haben und in dem
sie ihre Tochter pflegen. „Claudia braucht 24 Stunden
Betreuung. Sie kann selbst nichts mehr machen: sich
nicht drehen, nicht reden und essen“, sagten die Eltern,
die die Hoffnung auf Besserung allerdings noch nicht aufgegeben
haben.

Mit den nach der Serenade des Gesangvereins erbetenen
Spenden und dem Reinerlös des Festes wollen die
Obermeitinger die Familie Jakob unterstützen.

Das Benefizfest rund um den Feststadel zog mit seinem
bunten Programm viele Menschen an. Schon zum Gottesdienst
mit Pater Klaus Spiegel von der Erzabtei St.
Ottilien war der Stadel voll besetzt. Die Obermeitinger
Jugendblaskapelle unter der Leitung von Wolfgang Forster
übernahm die musikalische Gestaltung.

Beim anschließenden Frühschoppen spielte die Blaskapelle
Obermeitingen. Den ganzen Tag über gab es eine
große Auswahl an Essen und Getränken, am Nachmittag
fanden Kaffee und viele selbstgemachte Torten und Kuchen
reißenden Absatz.

An zahlreichen Spielstationen konnten sich Klein und
Groß in Glück und Geschicklichkeit üben und Preise gewinnen.
Für die Kinder gab es unter anderem eine Hüpfburg,
Schminken, Kutschfahrten und ein Piratenfest.

Mit einem mehr als zweistündigen Konzert und vielen
beliebten Polkas, Märschen und Stücken aus Film und
Popmusik, die man sich gegen eine Spende wünschen
konnte, beendete die Musikkapelle Obermeitingen unter
der Leitung von Daniela Rid am Abend das gelungene
Benefizfest.

![](/konzerte/2017/serenade/20170710-1939-DSC03960.jpg)
![](/konzerte/2017/serenade/20170723-1128-DSC03971.jpg)
![](/konzerte/2017/serenade/20170723-1139-DSC03982.jpg)
![](/konzerte/2017/serenade/20170723-1240-DSC04012.jpg)
![](/konzerte/2017/serenade/20170723-1620-DSC04065.jpg)
![](/konzerte/2017/serenade/20170723-1621-DSC04068.jpg)
![](/konzerte/2017/serenade/20170723-1811-DSC04099.jpg)
![](/konzerte/2017/serenade/20170723-2057-DSC04158.jpg)
