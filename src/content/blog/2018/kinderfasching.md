---
title: "Ein Obermeitinger Faschingshöhepunkt"
description: "Mit viel Herz und Einsatz machten die Mitglieder des Musikvereins Obermeitingen den Sonntagnachmittag wieder zu einem Höhepunkt in der närrischen Zeit für die faschingsbegeisterten Kinder aus Obermeitingen und Umgebung."
category_ref: "fasching"
pubDate: "2018-01-20T00:00:00+02:00"
heroImage: "konzerte/2018/fasching/DSCN0453.jpg"
---
Erst zogen die vielen kleinen und großen Besucher
eine halbe Stunde durch die Straßen Obermeitingens
und sammelten fleißig Süßigkeiten bei den Anwohnern
ein.

Dann ging es im Bürgerhaussaal mit der Faschingsparty
weiter. Die beiden Animateure auf der Bühne - Bauarbeiter
Georg Weihmayer und Hippiemädchen Vanessa
Waldheim – hatten für die kleinen Gäste lustige Tanzund
Partyspiele vorbereitet und alle machten begeistert
mit zum Beispiel beim Luftballon- und Zeitungstanz, bei
Macarena und dem Schaumkusswettessen.


DJ Sascha Ertle war wieder für die Musik zuständig und
verwandelte den abgedunkelten Saal mit Lichteffekten in
eine Kinderdisco.

Gegen den Hunger gab es selbstgemachten Kuchen und
Pommes.

Viel Applaus bekam die Kindergarde des Mittelstetter
Faschingsclubs für ihren zauberhaften Showtanz.

![](/konzerte/2018/fasching/DSCN0282.jpg)
![](/konzerte/2018/fasching/DSCN0298.jpg)
![](/konzerte/2018/fasching/DSCN0453.jpg)
![](/konzerte/2018/fasching/DSCN0509.jpg)
![](/konzerte/2018/fasching/DSCN0514.jpg)
![](/konzerte/2018/fasching/DSCN0538.jpg)
![](/konzerte/2018/fasching/DSCN0585.jpg)
![](/konzerte/2018/fasching/DSCN0593.jpg)
![](/konzerte/2018/fasching/DSCN0684.jpg)
