---
title: "Musik aus Kinoklassikern begeistert Publikum"
description: "Nach dem großen Zuspruch in den vergangenen Jahren und weil den Musikern dieses Genre einfach Freude macht, stand das Jahreskonzert des Musikvereins Obermeitingen auch heuer wieder ganz im Zeichen der Filmmusik."
category_ref: "in_konzert"
pubDate: "2018-01-01T00:00:00+02:00"
heroImage: "konzerte/2018/inkonzert/02.jpg"
---
Im restlos ausverkauften Bürgerhaussaal gaben sich
viele bekannte Kinohelden musikalisch die Ehre: Von
Harry Potter über Indiana Jones, E.T. und Forrest Gump
bis hin zu den Astronauten der Apollo 13. Sogar der
Weiße Hai kam vorbeigeschwommen.

Mit all den wundervollen Melodien aus diesen Kinoklassikern
unterhielt das symphonische
Blasorchester unter der Leitung von Daniela Rid die Zuhörer
wieder ausgezeichnet: wie gewohnt, mit viel Spielfreude
und auf hohem Niveau. Nicht umsonst tritt das
Orchester bei Wertungsspielen erfolgreich in der Oberstufe
an.

In einem wunderschönen Arrangement erfreuten die Musiker
das Publikum mit der Ballade „Somewhere“ aus
dem Musical „West Side Story“, das 1961 verfilmt wurde.
Schwungvoll ging es zu beim Besuch des „Weißen
Rössl“ am Wolfgangsee. Das Orchester und auch das
Publikum hatten sichtlich Spaß mit dem Medley aus den
bekanntesten Melodien der Operette von Ralf Benatzky,
die als Komödie 1961 in die Kinos kam.

Dass die wahren „Kings of Swing“ die Schlagzeuger
sind, zeigte das tolle Percussion-Ensemble eindrucksvoll
beim gleichnamigen Stück von Dick Ravenal und erntete
großen Applaus für seine solistischen Darbietungen.

Mit der „New York Overture“ des Niederländers Kees Vlak
nahmen die knapp 50 Musiker die Zuhörer mitten hinein in
die pulsierende US-Metropole.

Eingerahmt wurde das Konzert von zwei
beliebten Konzertmärschen, dem „Blauen Enzian“ von
Ernst Hoffmann und „Abel Tasman“ von Alexander Pfluger.
Für eine gelungene, kurzweilige Moderation
am Konzertabend sorgten die beiden Schlagwerker
Georg Weihmayer und Dominik Lauter.

Mit Norberts Gälles „Sorgenbrecher-Polka“ und „What a
wonderful world“ als gefühlvolle Zugaben entließ das
Orchester das begeisterte Publikum in die Nacht.

![](/konzerte/2018/inkonzert/02.jpg)
![](/konzerte/2018/inkonzert/07.jpg)
![](/konzerte/2018/inkonzert/13.jpg)
![](/konzerte/2018/inkonzert/16.jpg)
