---
title: "Serenade am See"
description: "Der Musikverein Obermeitingen veranstaltet am 28.07.2018 ab 19 Uhr wieder eine Serenade am Baggersee Obermeitingen."
category_ref: "serenade"
pubDate: "2018-07-21T00:00:00+02:00"
heroImage: "konzerte/2018/serenade/serenade_2018.jpg"
related_konzert_ref: "serenade/2018"

---
Entspannen Sie sich bei Latin-, Swing- und Pop-Musik wie Hawaii Five O, 80er Kulthits, Children of Sanchez, Kaiserin Sisi, Polka mit Herz, What A Wonderful World und vielen anderen mehr. Zusammen mit der Jugendkapelle und der Bläserklasse wird der Musikvereins Obermeitingen gemeinsam mit Ihnen den Sonntagabend ausklingen lassen.

Das bunte und vielfältige Programm sollten Sie nicht verpassen, es ist für jeden Geschmack etwas dabei!

Es wird fruchtig-sommerliche Getränke geben, sowie leckeres Finger-Food zur gemütlichen Atmosphäre. Feiern Sie mit uns auf den aufgestellten Bänken oder Ihrer mitgebrachten Picknickdecke.

Vanessa Waldheim
