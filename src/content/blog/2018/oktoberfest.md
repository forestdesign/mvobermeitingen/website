---
title: "Oktoberfest im Bürgerhaussaal"
description: "Am 6. Oktober veranstaltete der Musikverein Obermeitingen ein Oktoberfest mit kleinem Kinderprogramm."
category_ref: "allgemein"
pubDate: "2018-10-07T00:00:00+02:00"
heroImage: "konzerte/2018/oktoberfest/20181006-1945-DSC05292.jpg"
---
Ab 18 Uhr stand die Tür des festlich geschmückten Bürgerhaussaals für Groß und Klein offen. Für die Kinder gab es auf der Bühne ein paar Spiele, bei denen sie tolle Preise gewinnen konnten. Anschließend ging es an das „Gummibären-Glücksrad“ und es wurde begeistert gedreht.

Ab 19.30 Uhr unterhielt uns die „Blasmusiktruppe“ des Musikvereins Obermeitingen mit böhmisch-mährischer und bayerischer Blasmusik und heizte die Stimmung auf. Zu Beginn gab es wieder einen Bieranstich durch den 1. Bürgermeister Erwin Losert. Damit war das Oktoberfest offiziell eröffnet.

So gegen 21 Uhr konnten die Besucher am Glücksrad auch etwas gewinnen. Vom Freischnaps bis hin zum Geldgewinn war alles dabei und ging an gutgelaunte Gäste. Für alle anderen, gab es einen Trostpreis an der Schnapsbar.

Vanessa Waldheim

![](/konzerte/2018/oktoberfest/20181006-1944-DSC05278.jpg)
![](/konzerte/2018/oktoberfest/20181006-1945-DSC05292.jpg)
![](/konzerte/2018/oktoberfest/20181006-2007-DSC05299.jpg)
![](/konzerte/2018/oktoberfest/20181006-2010-DSC05317.jpg)
