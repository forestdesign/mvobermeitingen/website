---
title: "Vatertagsfest in Obermeitingen"
description: "Der Musikverein Obermeitingen öffnet die Tore zum Feststadl zum Vatertagsfest an Christi Himmelfahrt, den 30.Mai."
category_ref: "serenade"
pubDate: "2019-05-20T00:00:00+02:00"
heroImage: "konzerte/2019/vatertag/vatertagsfest_2019.jpg"
related_konzert_ref: "vatertag/2019"
---

Mit Gottesdienst, Musik, herzhaften Leckereien und Spaß für die ganze Familie verbringen wir gemeinsam einen schönen Tag. Beginn ist um 9.30 Uhr mit Flurumgang und anschließendem Gottesdienst im Feststadl in Obermeitingen. Ab 11.30 Uhr bis zum Nachmittag unterhalten Sie die Musikvagabunden mit buntem Programm aus Polkas, Märschen und böhmisch-mährischer Blasmusik.  Für die Kleinen wird ein Kinderprogramm mit verschiedenen Spielen und einem Kennenlernen der Musikinstrumente geboten.

Vanessa Waldheim

