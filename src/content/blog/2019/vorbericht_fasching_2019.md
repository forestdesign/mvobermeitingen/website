---
title: "Kinderfasching am 24. Februar in Obermeitingen" 
description: "Am 24. Februar veranstaltet der Musikverein Obermeitingen wieder den alljährlichen Kinderfasching. Ab 13.30 Uhr wird es einen Umzug mit Garde und Sambatrommelgruppe durch Obermeitingen geben.  Der Musikverein Obermeitingen lädt Sie herzlich zum Umzug ein, der am Dorfplatz um 13.15 Uhr beginnt. Der Musikverein freut sich über jeden, der mitläuft und Süßigkeiten wirft."
category_ref: "serenade"
pubDate: "2019-05-20T00:00:00+02:00"
heroImage: "konzerte/2019/fasching/kinderfasching_2019.jpg"
related_konzert_ref: "fasching/2019"
---



Vanessa Waldheim
