---
title: "Serenade am Rathausplatz"
description: "Der Musikverein Obermeitingen veranstaltet am 25.07.2021 ab 19 Uhr endlich wieder eine Serenade am Rathausplatz Obermeitingen."
category_ref: "serenade"
pubDate: "2021-07-20T00:00:00+02:00"
heroImage: "konzerte/2021/serenade/Plakat-Serenade-Rathausplatz.jpg"
related_konzert_ref: "serenade/2021"
---

Entspannt Euch bei traditioneller Blasmusik, Latin- und Pop-Musik, sowie „Best of Beatles“, „Latin Gold“ und vielen anderen mehr. Zusammen mit der Jugendkapelle wird der Musikverein Obermeitingen gemeinsam mit Euch den Sonntagabend ausklingen lassen.

Das bunte und vielfältige Programm solltet Ihr nicht verpassen, es ist für jeden Geschmack etwas dabei!

Endlich dürfen wir wieder musizieren und wollen dies mit Euch feiern. Bitte kommt etwas früher wegen der Platzvergabe und der Kontaktdatenaufnahme (schriftlich oder per App).

Vanessa Waldheim
