---
title: "Neuer Dirigent in Obermeitingen"
description: "Seit Januar diesen Jahres leitet der Augsburger Bob Sibich das sinfonische Blasorchester des Musikvereins Obermeitingen."
category_ref: "allgemein"
pubDate: "2020-01-01T00:00:00+01:00"
heroImage: "neuigkeiten/2020/Bob-Sibich.jpg"
---
„Mehr fortissimo und viel mehr Akzente – das will ich von
den Trompeten und Posaunen hören.“ Wir sind in der
wöchentlichen Probe des sinfonischen Blasorchesters
des Musikvereins Obermeitingen. Vorne am Dirigentenpult
sitzt Robert, genannt Bob, Sibich auf einem Hocker
und wiederholt mit den Bläsern immer wieder die gleichen
Fanfarenklänge. „Die Rhythmik muss einfach gut
sitzen“, ruft er den Musikern zu. Denn schließlich soll „A
little opening“ von Thiemo Kraas beim Konzert am Ostermontag
perfekt klingen.

Bob Sibich ist der neue musikalische Leiter des bei
Wettbewerben in der Oberstufe spielenden Orchesters.
Er hat im Januar den Taktstock von Daniela Rid übernommen,
die sich nach knapp neun Jahren am Dirigentenpult
mit einem bejubelten Konzert im Dezember verabschiedete.

Der 33-Jährige Augsburger hat an der Musikhochschule
Augsburg-Nürnberg Tuba und Blasorchesterleitung studiert.
Mit acht Jahren bekam er den ersten Tubaunterricht.
Schon zwei Jahre später spielte er in der Harmoniemusik
Welden, dessen Leitung er 2008 übernahm.
„Meine zwei großen Brüder spielten Instrumente, das
wollte ich auch“, erzählt Sibich. Und wie kam er auf die
Tuba? „Der netteste Mann bei der Harmoniemusik hat
Tuba gespielt, da habe ich gedacht, da setze ich mich
daneben“, erklärt Sibich lachend seine Instrumentenwahl.

Als Bob Sibich seine vielen musikalischen Aktivitäten
aufzählt, scheint es, seine Woche hätte mehr als sieben
Tage und sein Tag mehr als 24 Stunden. Er ist Mitbegründer
mehrerer Ensembles, wie der Brass Band
„Woodshockers“, der Bläserphilharmonie Ehgatten und
des Fanfare-Orchesters Bayern. Die beiden Letztgenannten
leitet er heute noch. Zudem hat er 2018 die musikalische
Leitung des Musikvereins Fischach übernommen
und ist Registerführer beim sinfonischen Blasorchester
Vorarlberg. Daneben spielt er Tuba in der Bayerischen
Brass Band Akademie, kurz 3BA, und ist als Musikpädagoge
an verschiedenen Musikschulen tätig.

Und wie kam er nach Obermeitingen? „Im vergangenen
Jahr bin ich hier mal als Gastdirigent eingesprungen.
Dann rief mich Michael Weihmayer an, dass der Musikverein
ab Januar einen neuen Dirigenten sucht. Und
nach sieben Jahren beim Musikverein Fahlheim hatte ich
Lust auf etwas Neues“, so Sibich.

Mit seinen Bläserensembles konnte er schon viele Erfolge
feiern. Das liegt unbestritten auch an seinem hohen
Anspruch an sich und an die Musiker.

Diesen bekommen auch die Obermeitinger bei der Probe
zu spüren. „Ich mache das Tempo“, ruft Sibich in die
Runde und scheint jedes einzelne Instrument im Blick
und im Ohr zu haben. „Ich bin mal gespannt, ob die Musiker
das mitmachen“, sagt der Dirigent, der sich bewusst
ist, dass sich die Obermeitinger erst noch an ihren
neuen Leiter gewöhnen müssen.
