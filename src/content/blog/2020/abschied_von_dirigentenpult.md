---
title: "Abschied vom Dirigentenpult"
description: "Am Samstag, den 7. Dezember, ab 20 Uhr, stand Daniela Rid nach knapp neun Jahren zum letzten Mal am Dirigentenpult des symphonischen Blasorchesters des Musikvereins Obermeitingen."
category_ref: "allgemein"
pubDate: "2020-01-01T00:00:00+01:00"
heroImage: "neuigkeiten/2020/Daniela-Rid.jpg"
---
„Ich habe ein lachendes und ein
weinendes Auge, denn ich dirigiere
schon gerne“, erzählt die
Hurlacherin im Gespräch. „Ich
genieße jedes Mal aufs Neue
das Erfolgserlebnis eines gelungenen
Konzerts.“

Was sind ihre Pläne nach dem Konzert, das der Musikverein
liebevoll unter den Titel „Dankeschön Daniela“
gestellt hat? Und wie kam die gebürtige Oberallgäuerin
zur Leitung des Orchesters und worauf legte sie in dieser
Zeit besonders Wert?

Wie so oft, liegen die Wurzeln einer musikalischen Laufbahn
in der Familie. „Wir waren vier Kinder daheim und
jeder hat mindestens zwei Instrumente gelernt“, berichtet
die 42-Jährige, die aus Rettenberg stammt. Im Alter von
sieben Jahren begann sie mit der Harfe. Zwei Exemplare
stehen auch heute in ihrem Musikzimmer. Mit zwei Geschwistern
machte sie als Trio Stubenmusik. „Dann kamen
die Querflöte und das Baritonsaxophon dazu und
ich spielte in der Musikkapelle Rettenberg.“ 1997 absolvierte
sie einen Dirigentenkurs beim Allgäu-
Schwäbischen Musikbund (ASM).

Nach Hurlach hat Daniela Rid die Liebe verschlagen. Bei
einer Geburtstagsfeier in der Hurlacher Verwandtschaft
lernte sie 1998 ihren Ehemann Johann kennen, der dort
mit der Band „Die Hurlacher“ ein Ständchen spielte.
2009 wurde geheiratet.

Im Jahr 2011 war der Musikverein Obermeitingen nach
dem Weggang von Rainer Kropf auf der Suche nach
einer neuen Leitung und fragte bei Daniela Rid an.
„Eigentlich wollte ich nur im Orchester mitspielen“, erzählt
sie, aber ehe sie sich versah, stand sie mit dem
Taktstock vor dem Orchester.

„Wir mussten uns erst einmal dialekttechnisch einander
angleichen“, blickt Rid mit einem Lachen auf die
„Anfangsschwierigkeiten“ als Dirigentin in Obermeitingen
zurück. „Wichtig war mir immer die richtige Wahl der Stücke
aus den verschiedensten Musikrichtungen. Das Publikum
sollte sich abgeholt fühlen“, führt Daniela Rid an.
So hat sich das Orchester ein breites Repertoire von
Märschen und Polkas über konzertante Orchesterwerke
bis hin zur Operetten- und Filmmusik erarbeitet. „Ich lege
nicht nur Wert auf die technische Ausführung, sondern
vor allem auf den Ausdruck, damit beim Publikum auch
emotional etwas ankommt“, betont die dreifache Mama,
die nur einige Monate nach den Geburten der Kinder
pausiert hatte.

Doch warum gibt sie nun den Taktstock ab? „Zu Beginn
habe ich gesagt, nach maximal zehn Jahren höre ich
auf. Nun kennen sie schon alle meine Witze“, bemerkt
die Musikerin lachend. „Das Blasorchester Obermeitingen
ist meine musikalische Heimat, nun habe ich nach
fast neun Jahren als Dirigentin einfach wieder Lust zum
Spielen.“ „Eigentlich ist die Querflöte mein Instrument,
aber im Moment versuche ich mich an der Oboe, die gibt
es in unserem Orchester noch nicht“, verrät Daniela Rid.
Doch auch sonst wird es der lebensfrohen Hurlacherin
sicher nicht langweilig. Ihre zwei Buben und die Tochter
halten sie auf Trab und sie leitet den Hurlacher Kirchenchor
St. Laurentius. Neben Familie und Musik arbeitet
die Diplom-Betriebswirtin im Homeoffice für ein Allgäuer
Maschinenbauunternehmen.
