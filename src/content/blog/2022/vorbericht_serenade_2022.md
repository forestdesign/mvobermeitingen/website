---
title: "Serenade am Rathausplatz"
description: "Der Musikverein Obermeitingen veranstaltet am 17.07.2022 ab 18 Uhr wieder eine Serenade am Rathausplatz in Obermeitingen."
category_ref: "serenade"
pubDate: "2022-07-01T00:00:00+02:00"
heroImage: "konzerte/2022/serenade/2022-Serenade-Rathausplatz.jpg"
related_konzert_ref: "serenade/2022"
---

Entspannen Sie sich bei Latin-, Swing-, Film- und Pop-Musik wie Spanish Fever, Guardians of he Galaxy, The Blues Brothers Revue, Abba on Broadway und vielen anderen mehr. Zusammen mit der Jugendkapelle wird der Musikvereins Obermeitingen gemeinsam mit Ihnen den Sonntagabend ausklingen lassen.

Das bunte und vielfältige Programm sollten Sie nicht verpassen, es ist für jeden Geschmack etwas dabei!

Es wird erfrischende Getränke und Cocktails geben zur gemütlichen Atmosphäre. Feiern Sie mit uns.

Vanessa Waldheim
