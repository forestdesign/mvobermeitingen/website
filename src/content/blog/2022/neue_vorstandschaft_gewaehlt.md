---
title: "Neue Vorstandschaft gewählt"
description: "Am 23. Juni 2022 wurde im Rahmen der Mitgliederversammlung die neue Vorstandschaft des Musikvereins Obermeitingen gewählt."
category_ref: "allgemein"
pubDate: "2020-01-01T00:00:00+01:00"
heroImage: "mvo_2022_vorstandschaft.jpg"
---
Wir verabschieden und bedanken uns recht herzlich für die jahrelange Mitarbeit und Unterstützung bei Helmut Knie als 2. Vorstand und Verena Schmid, Katharina Huber und Thomas Schiegg als Beisitzer.

Wir gratulieren der neuen Vorstandschaft zur Wahl und wünschen einen guten Start.

Vanessa Waldheim

Von links nach rechts :
Olaf Waldheim, Vincent Weihmayer, Sabine Gerlach (Kassiererin), Vanessa Waldheim (Schriftführerin), Michaela Lüdecke (3. Vorstand), Georg Weihmayer (2. Vorstand), Katharina Jakob, Michael Weihmayer (1.Vorstand)
