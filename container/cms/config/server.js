module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  app: {
    keys: env.array('APP_KEYS'),
  },
  webhooks: {
    populateRelations: env.bool('WEBHOOKS_POPULATE_RELATIONS', false),
  },
  // konfiguriert die cors-Middleware, um alle Anfragen zuzulassen
  // von jeder Domain, jedem Port und jedem Protokoll
  // (dies ist unsicher und sollte nicht in einer Produktionsumgebung verwendet werden)
  cors: {
    enabled: true,
    origin: env('CORS_ORIGIN', '*'),
    headers: ['Content-Type', 'Authorization'],
    exposedHeaders: [],
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH'],
    credentials: true,
    maxAge: 86400,
  },
  settings: {

    csp: {
      enabled: true,
      policy: [
        "default-src 'self'",
        `connect-src 'self' http://localhost:1337 ws://localhost:1337`,
        // fügen Sie hier andere CSP-Richtlinien hinzu, die Sie benötigen
      ],
    },
  }
})
