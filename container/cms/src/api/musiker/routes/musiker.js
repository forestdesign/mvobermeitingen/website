'use strict';

/**
 * musiker router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::musiker.musiker');
