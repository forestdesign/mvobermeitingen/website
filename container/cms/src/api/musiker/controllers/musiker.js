'use strict';

/**
 * musiker controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::musiker.musiker');
