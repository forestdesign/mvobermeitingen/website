'use strict';

/**
 * vorstand router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::vorstand.vorstand');
