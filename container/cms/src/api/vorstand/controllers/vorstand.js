'use strict';

/**
 * vorstand controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::vorstand.vorstand');
