'use strict';

/**
 * news-kategorie controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::news-kategorie.news-kategorie');
