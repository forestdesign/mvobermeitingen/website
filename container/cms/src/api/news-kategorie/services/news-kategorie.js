'use strict';

/**
 * news-kategorie service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::news-kategorie.news-kategorie');
