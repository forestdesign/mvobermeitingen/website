'use strict';

/**
 * news-kategorie router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::news-kategorie.news-kategorie');
