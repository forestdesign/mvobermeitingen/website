'use strict';

/**
 * rollen controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::rollen.rollen');
